package com.alienlabz.encomendaz.exceptions;

import java.util.ResourceBundle;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;

import br.gov.frameworkdemoiselle.util.Beans;

import com.alienlabz.encomendaz.exception.CodeExistsException;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;

/**
 * Trata a forma como a exceção será exibida para o usuário.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class ExceptionHandler {

	private static ConstraintViolationException getConstraintException(Throwable cause) {
		ConstraintViolationException result = null;
		if (cause != null && cause instanceof ConstraintViolationException) {
			result = (ConstraintViolationException) cause;
		} else if (cause != null) {
			result = getConstraintException(cause.getCause());
		}
		return result;
	}

	/**
	 * Tratar a exceção lançada quando já existe um código cadastrado.
	 * 
	 * @param exception Exceção.
	 */
	public static void handle(final CodeExistsException exception) {
		final ResourceBundle bundle = ResourceBundleFactory.getMessagesBundle();
		showMessage(bundle.getString("exception.code.exists"));
	}

	/**
	 * Tratar a exceção e exibir alguma mensagem para o usuário.
	 * 
	 * @param exception Exceção a ser tratada.
	 */
	public static void handle(final ConstraintViolationException exception) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				StringBuilder message = new StringBuilder();
				message.append("<html>");
				message.append("<strong>Existem campos com dados incorretos:</strong><br/>");
				for (ConstraintViolation<?> constraint : exception.getConstraintViolations()) {
					message.append(constraint.getMessage());
					message.append("<br/>");
				}
				message.append("</html>");
				showMessage(message.toString());
			}

		});
	}

	/**
	 * Tratar a exceção e exibir alguma mensagem para o usuário.
	 * 
	 * @param exception Exceção a ser tratada.
	 */
	public static void handle(final Throwable exception) {
		Logger logger = Beans.getReference(Logger.class);
		logger.error("Erro", exception);
		ConstraintViolationException violation = getConstraintException(exception);
		if (violation != null) {
			handle(violation);
		} else if (exception instanceof CodeExistsException) {
			handle((CodeExistsException) exception);
		} else {
			showMessage(exception.getMessage());
		}
	}

	private static void showMessage(final String message) {
		final ResourceBundle bundle = ResourceBundleFactory.getMessagesBundle();
		JOptionPane.showMessageDialog(null, message, bundle.getString("exception.generic.title"),
				JOptionPane.ERROR_MESSAGE);
	}

}
