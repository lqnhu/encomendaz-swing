package com.alienlabz.encomendaz.bus;

import java.lang.annotation.Annotation;

import javax.enterprise.inject.Any;
import javax.enterprise.util.AnnotationLiteral;

import br.gov.frameworkdemoiselle.util.Beans;

/**
 * Gerenciador de eventos que tratam da comunicação entre View e Presenter.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@SuppressWarnings("serial")
public class EventManager {

	/**
	 * Disparar um determinado evento.
	 * 
	 * @param event Evento a ser lançado.
	 */
	public static void fire(final Object event) {
		Beans.getBeanManager().fireEvent(event, new AnnotationLiteral<Any>() {
		});
	}

	public static void fire(final Object event, final Annotation... qualifiers) {
		Beans.getBeanManager().fireEvent(event, qualifiers);
	}

}
