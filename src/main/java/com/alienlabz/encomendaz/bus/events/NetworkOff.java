package com.alienlabz.encomendaz.bus.events;

/**
 * Evento lançado para informar que a rede está fora do ar.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class NetworkOff {

}
