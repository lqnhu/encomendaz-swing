package com.alienlabz.encomendaz.bus.events;

/**
 * Evento lançado para solicitar o teste do servidor de e-mail selecionado.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class TestEmailConnection {
}
