package com.alienlabz.encomendaz.bus.events;

/**
 * Evento lançado quando se quer importar um lote de códigos separados por um caracter.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class BatchCodesInsertion {
	private String separator;
	private String codes;

	public BatchCodesInsertion(String codes, String separator) {
		this.codes = codes;
		this.separator = separator;
	}

	public String getSeparator() {
		return separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getCodes() {
		return codes;
	}

	public void setCodes(String codes) {
		this.codes = codes;
	}

}
