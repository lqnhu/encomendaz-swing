package com.alienlabz.encomendaz.bus.events;

/**
 * Evento lançado sempre que há necessidade de informar ao usuário que o sistema se encontra ocupado.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class Working {

}
