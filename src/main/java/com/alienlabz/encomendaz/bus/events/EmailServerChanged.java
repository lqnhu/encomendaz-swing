package com.alienlabz.encomendaz.bus.events;

import com.alienlabz.encomendaz.domain.EmailServers;
import com.alienlabz.encomendaz.view.preferences.PreferencesEmail;


/**
 * Evento lançado quando o usuário modifica o servidor de e-mail que deseja.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class EmailServerChanged {
	private EmailServers server;
	private PreferencesEmail preferencesEmail;

	public EmailServerChanged(PreferencesEmail panel, EmailServers server) {
		this.setServer(server);
		this.setPreferencesEmail(panel);
	}

	public void setServer(EmailServers server) {
		this.server = server;
	}

	public EmailServers getServer() {
		return server;
	}

	public void setPreferencesEmail(PreferencesEmail preferencesEmail) {
		this.preferencesEmail = preferencesEmail;
	}

	public PreferencesEmail getPreferencesEmail() {
		return preferencesEmail;
	}
}
