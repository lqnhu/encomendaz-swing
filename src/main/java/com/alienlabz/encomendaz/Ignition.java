package com.alienlabz.encomendaz;

import gr.zeus.ui.JSplash;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.jboss.weld.environment.se.StartMain;
import org.jboss.weld.environment.se.events.ContainerInitialized;
import org.jdesktop.swingx.JXTipOfTheDay;
import org.jdesktop.swingx.tips.TipLoader;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;
import org.slf4j.Logger;

import br.gov.frameworkdemoiselle.annotation.Name;

import com.alienlabz.encomendaz.exceptions.UIException;
import com.alienlabz.encomendaz.message.Messages;
import com.alienlabz.encomendaz.preferences.UserPreferences;
import com.alienlabz.encomendaz.presenter.MainPresenter;
import com.alienlabz.encomendaz.scheduler.Quartz;
import com.alienlabz.encomendaz.view.tray.TrayIconManager;

/**
 * Inicia a interface Swing do EncomendaZ Cheetah.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class Ignition {

	@Inject
	@Name("system-core")
	private ResourceBundle bundleCore;

	@Inject
	@Name("messages")
	private ResourceBundle bundleMessages;

	@Inject
	@Name("images")
	private ResourceBundle bundleImages;

	@Inject
	private UserPreferences prefs;

	@Inject
	private Logger logger;

	@Inject
	private MainPresenter entrancePresenter;

	private JSplash splash;

	/**
	 * Trata o evento de inicialização do container do Weld para iniciar o sistema.
	 * 
	 * @param event Evento.
	 */
	public void start(@Observes final ContainerInitialized event) {
		logger.debug("inicializando o sistema.");

		initializeSplash();
		initializeEventQueue();

		try {
			logger.debug("inicializando com a skin: " + prefs.getDefaultSkin());
			SubstanceLookAndFeel.setSkin(prefs.getDefaultSkin());
			UIManager.getLookAndFeelDefaults().addResourceBundle("messages");
		} catch (final Exception e) {
			Messages.error(bundleMessages.getString("exception.lookandfeel"));
			throw new UIException();
		}

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				logger.debug("inicializando as tarefas de agendamento.");
				Quartz.initialize();

				logger.debug("inicializando o ícone no tray.");
				TrayIconManager.start();

				splash.splashOff();

				logger.debug("inicializando a exibição da tela inicial do sistema.");
				entrancePresenter.showEntrance();

				logger.debug("inicializando a exibição das dicas do dia.");
				initializeTipOfTheDay();
			}

		});
	}

	/**
	 * Inicializar o componente de exibição de dicas do dia.
	 */
	private void initializeTipOfTheDay() {
		Properties tips = new Properties();
		try {
			tips.load(getClass().getClassLoader().getResourceAsStream("tips.properties"));
			JXTipOfTheDay tipoOfTheDay = new JXTipOfTheDay(TipLoader.load(tips));
			tipoOfTheDay.showDialog(entrancePresenter.getEntrance(),
					Preferences.userNodeForPackage(UserPreferences.class));
		} catch (FileNotFoundException e) {
			Messages.error(bundleMessages.getString("exception.tips.day"));
		} catch (IOException e) {
			Messages.error(bundleMessages.getString("exception.tips.day"));
		}
	}

	/**
	 * Inicializar a tela de splash da aplicação.
	 */
	private void initializeSplash() {
		splash = new JSplash(Ignition.class.getClassLoader().getResource(
				"images/" + bundleImages.getString("splash.image")), false, false, false,
				bundleCore.getString("version"), new Font("Tahoma", 0, 11), Color.RED);
		splash.splashOn();
		try {
			Thread.sleep(5000);
		} catch (Exception e) {
		}
	}

	/**
	 * Inicializar o EventQueue para tratar exceções que não foram capturadas.
	 */
	private void initializeEventQueue() {
		EventQueue queue = Toolkit.getDefaultToolkit().getSystemEventQueue();

		logger.debug("registrando o evento de exceção no EventQueue do AWT.");
		queue.push(new EventQueue() {
			protected void dispatchEvent(AWTEvent newEvent) {
				logger.debug("tratando exceção lançada mas não capturada.");
				try {
					super.dispatchEvent(newEvent);
				} catch (Exception t) {
					String message = t.getMessage();

					if (message == null || message.length() == 0) {
						message = t.getClass().toString();
					}
					t.printStackTrace();
					Messages.fatal(message, t, getClass());
					System.exit(0);
				}
			}
		});
	}

	public static void main(final String... args) {
		createLogDirectory();
		new StartMain(args).go();
	}

	private static void createLogDirectory() {
		File dir = new File("./EncomendaZ/Log");
		if (!dir.exists()) {
			dir.mkdir();
		}
	}

}
