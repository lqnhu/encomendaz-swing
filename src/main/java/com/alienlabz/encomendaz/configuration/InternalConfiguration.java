package com.alienlabz.encomendaz.configuration;

import java.awt.Color;
import java.awt.Dimension;

/**
 * Obter as configurações internas da aplicação, como tamanho dos ícones, versão e etc.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class InternalConfiguration {

	/**
	 * Cor padrão do texto dos botões.
	 * 
	 * @return
	 */
	public static Color getDefaultButtonColor() {
		return Color.WHITE;
	}

	/**
	 * Obter as dimensões para um ícone do ribbon.
	 * 
	 * @return Dimensão.
	 */
	public static Dimension getDefaultRibbonIconDimension() {
		return new Dimension(64, 64);
	}

	/**
	 * Obter a dimensão padrão para a janela de entrada.
	 * 
	 * @return Dimensão.
	 */
	public static Dimension getEntranceDimension() {
		return new Dimension(1150, 800);
	}

	/**
	 * Obter a dimensão padrão da janela de cadastro de pessoas.
	 * 
	 * @return Dimensão.
	 */
	public static Dimension getPersonFormDimension() {
		return new Dimension(500, 480);
	}

	/**
	 * Obter a dimensão padrão da janela de cadastro de rastreios.
	 * 
	 * @return Dimensão.
	 */
	public static Dimension getTrackingFormDimension() {
		return new Dimension(500, 540);
	}

	public static Dimension getMapWindowDimension() {
		return new Dimension(800, 600);
	}

	public static Dimension getSearchFormDimension() {
		return new Dimension(900, 600);
	}

}
