package com.alienlabz.encomendaz.view.renderer;

import java.awt.Component;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;


import org.pushingpixels.substance.api.renderers.SubstanceDefaultComboBoxRenderer;

import com.alienlabz.encomendaz.util.IconUtil;

/**
 * Renderizador de combobox que possui uma imagem.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class PostalServicesComboRenderer extends SubstanceDefaultComboBoxRenderer {

	public PostalServicesComboRenderer(JComboBox combo) {
		super(combo);
	}

	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		if (value != null) {
			label.setIcon(IconUtil.getImageIcon(value.toString() + ".png"));
		}
		return label;
	}

}
