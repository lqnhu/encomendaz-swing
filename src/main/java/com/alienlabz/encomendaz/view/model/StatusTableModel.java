package com.alienlabz.encomendaz.view.model;

import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;


import org.alfredlibrary.formatadores.Data;

import com.alienlabz.encomendaz.domain.TrackingStatus;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;

/**
 * Table Model para exibição dos detalhes das encomendas.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class StatusTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	private final List<TrackingStatus> statuses;
	private final ResourceBundle messages;

	public StatusTableModel(final List<TrackingStatus> statuses) {
		this.statuses = statuses;
		if (statuses != null) {
			fireTableRowsInserted(0, statuses.size());
		}
		messages = ResourceBundleFactory.getMessagesBundle();
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public String getColumnName(final int column) {
		switch (column) {
		case 0:
			return messages.getString("table.column.status.date.label");
		case 1:
			return messages.getString("table.column.status.place.label");
		case 2:
			return messages.getString("table.column.status.status.label");
		case 3:
			return messages.getString("table.column.status.details.label");
		}
		return "";
	}

	@Override
	public int getRowCount() {
		return statuses.size();
	}

	@Override
	public Object getValueAt(final int row, final int column) {
		final TrackingStatus status = statuses.get(row);
		switch (column) {
		case 0:
			return Data.formatar(status.getDate(), "dd/MM/yyyy - HH:mm");
		case 1:
			return status.getPlace();
		case 2:
			return status.getStatus();
		case 3:
			return status.getDetails();
		}
		return null;
	}

}
