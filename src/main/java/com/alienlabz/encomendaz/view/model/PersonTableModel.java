package com.alienlabz.encomendaz.view.model;

import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import com.alienlabz.encomendaz.domain.Person;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;


/**
 * Model que exibirá os detalhes de @{link Person}.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
final public class PersonTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	private final List<Person> people;
	private ResourceBundle bundleMessages;
	private ResourceBundle bundleImages;

	public PersonTableModel(final List<Person> people) {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		bundleImages = ResourceBundleFactory.getImagesBundle();
		this.people = people;
		if (people != null) {
			fireTableRowsInserted(0, people.size());
		}
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public String getColumnName(final int column) {
		switch (column) {
		case 0:
			return "";
		case 1:
			return bundleMessages.getString("table.person.column.header.name");
		case 2:
			return bundleMessages.getString("table.person.column.header.email");
		case 3:
			return bundleMessages.getString("table.person.column.header.postalCode");
		default:
			return "";
		}
	}

	@Override
	public int getRowCount() {
		return people.size();
	}

	public Person getPerson(final int row) {
		Person result = null;
		if (row >= 0) {
			result = people.get(row);
		}
		return result;
	}

	@Override
	public Object getValueAt(final int row, final int column) {
		final Person person = people.get(row);
		Object result = null;
		switch (column) {
		case 0:
			result = bundleImages.getString("table.icon.person");
			break;
		case 1:
			result = person.getName();
			break;
		case 2:
			result = person.getEmail();
			break;
		case 3:
			result = person.getPostalCode();
			break;
		}
		return result;
	}
}
