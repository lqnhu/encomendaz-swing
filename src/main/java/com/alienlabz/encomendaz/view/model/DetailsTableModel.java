package com.alienlabz.encomendaz.view.model;

import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;
import com.alienlabz.encomendaz.util.StateUtil;


/**
 * Table Model para exibição das encomendas de modo detalhado.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class DetailsTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	private final List<Tracking> trackings;
	private ResourceBundle bundleMessages;

	public DetailsTableModel(final List<Tracking> trackings) {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		this.trackings = trackings;
		if (trackings != null) {
			fireTableRowsInserted(0, trackings.size());
		}
	}

	@Override
	public int getColumnCount() {
		return 8;
	}

	@Override
	public String getColumnName(final int column) {
		switch (column) {
		case 0:
			return bundleMessages.getString("table.column.header.status");
		case 1:
			return bundleMessages.getString("table.column.header.origin");
		case 2:
			return bundleMessages.getString("table.column.header.code");
		case 3:
			return bundleMessages.getString("table.column.header.sent");
		case 4:
			return bundleMessages.getString("table.column.header.lastchange");
		case 5:
			return bundleMessages.getString("table.column.header.elapsed");
		case 6:
			return bundleMessages.getString("table.column.header.people");
		case 7:
			return bundleMessages.getString("table.column.header.description");
		default:
			return "";
		}
	}

	@Override
	public int getRowCount() {
		return trackings.size();
	}

	public Tracking getTracking(final int row) {
		Tracking result = null;
		if (row >= 0) {
			result = trackings.get(row);
		}
		return result;
	}

	@Override
	public Object getValueAt(final int row, final int column) {
		final Tracking tracking = trackings.get(row);
		Object result = null;
		switch (column) {
		case 0:
			result = StateUtil.getStateIcon(tracking);
			break;
		case 1:
			if (IconUtil.existsIcon(tracking.getCountryAcronym() + ".png")) {
				result = tracking.getCountryAcronym() + ".png";
			} else {
				result = "unknown.png";
			}
			break;
		case 2:
			result = tracking;
			break;
		case 3:
			result = tracking.getSent();
			break;
		case 4:
			if (tracking.getLastStatus() != null) {
				result = tracking.getLastStatus().getDate();
			}
			break;
		case 5:
			result = tracking;
			break;
		case 6:
			result = tracking;
			break;
		case 7:
			result = tracking.getDescription();
			break;
		}

		return result;
	}
}
