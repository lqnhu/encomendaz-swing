package com.alienlabz.encomendaz.view.model;

import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.MutableComboBoxModel;

import com.alienlabz.encomendaz.domain.PostalService;


/**
 * Model para a lista de seleção de serviços postais.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class PostalServiceComboBoxModel extends AbstractListModel implements MutableComboBoxModel {
	private static final long serialVersionUID = 1L;
	private List<PostalService> list;
	private PostalService selected;

	@Override
	public Object getElementAt(final int position) {
		Object result = null;
		try {
			result = list.get(position);
		} catch (IndexOutOfBoundsException exception) {

		}
		return result;
	}

	@Override
	public Object getSelectedItem() {
		return selected;
	}

	@Override
	public int getSize() {
		return list.size();
	}

	/**
	 * Definir a lista de serviços postais que aparecerão no combobox.
	 * 
	 * @param postalService Serviços Postais.
	 */
	public void setPostalServices(final List<PostalService> postalService) {
		this.list = postalService;
	}

	@Override
	public void setSelectedItem(final Object selection) {
		if (selection instanceof String) {
			if ("".equals(selection)) {
				selected = null;
			} else {
				selected = new PostalService();
				selected.setName(selection.toString());
			}
		} else {
			selected = (PostalService) selection;
		}
		fireContentsChanged(this, -1, list.size());
	}

	@Override
	public void addElement(Object element) {
		list.add((PostalService) element);
		int length = getSize();
		fireIntervalAdded(this, length - 1, length - 1);
	}

	@Override
	public void insertElementAt(Object element, int index) {
		list.add(index, (PostalService) element);
		fireIntervalAdded(this, index, index);
	}

	@Override
	public void removeElement(Object element) {
		int index = list.indexOf(element);
		if (index != -1) {
			list.remove(element);
			fireIntervalRemoved(this, index, index);
		}
	}

	@Override
	public void removeElementAt(int index) {
		if (getSize() >= index) {
			list.remove(index);
			fireIntervalRemoved(this, index, index);
		}
	}

}
