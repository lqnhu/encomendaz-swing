package com.alienlabz.encomendaz.view.table.cellrenderer;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;


import org.pushingpixels.substance.api.renderers.SubstanceDefaultTableCellRenderer;

import com.alienlabz.encomendaz.util.IconUtil;

/**
 * Renderizador para celulas que contenham ícones.
 * 
 * @author Marlon Silva Carvalho
 * @since 2.0.0
 */
@SuppressWarnings("serial")
public class IconCellRenderer extends SubstanceDefaultTableCellRenderer {

	public IconCellRenderer() {
		this.setHorizontalAlignment(CENTER);
	}

	@Override
	public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {

		final JLabel lbl = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		lbl.setHorizontalAlignment(SwingConstants.CENTER);
		try {
			lbl.setIcon(IconUtil.getImageIcon(value.toString()));
		} catch (Exception exception) {

		}
		lbl.setText("");
		return lbl;
	}

}
