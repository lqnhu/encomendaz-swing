package com.alienlabz.encomendaz.view.table.cellrenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import org.alfredlibrary.formatadores.Data;
import org.pushingpixels.substance.api.renderers.SubstanceDefaultTableCellRenderer;

/**
 * Renderizador da célula que mostra a data de envio, data da última situação e tempo em trânsito.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class TimeCellRenderer extends SubstanceDefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;

	@Override
	public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
		final Date date = (Date) value;

		final JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		label.setText(Data.formatar(date, "dd/MM/yyyy"));
		label.setFont(Font.decode("Tahoma-NORMAL-11"));
		label.setForeground(Color.DARK_GRAY);
		label.setHorizontalAlignment(SwingConstants.CENTER);

		return label;
	}
}
