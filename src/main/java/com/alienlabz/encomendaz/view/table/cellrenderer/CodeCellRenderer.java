package com.alienlabz.encomendaz.view.table.cellrenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;


import org.pushingpixels.substance.api.renderers.SubstanceDefaultTableCellRenderer;

import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;

/**
 * Renderizador da célula que contém o código da postagem.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class CodeCellRenderer extends SubstanceDefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;

	@Override
	public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {

		final Tracking tracking = (Tracking) value;
		final JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

		label.setHorizontalTextPosition(JLabel.CENTER);
		label.setText("<html><center>" + tracking.getCode()
				+ (tracking.isTaxed() ? "<br><font size='2' color='red'>" + ResourceBundleFactory.getMessagesBundle().getString("default.text.taxed") + "</font>" : "")
				+ " </center></html>");

		label.setForeground(Color.DARK_GRAY);
		label.setFont(Font.decode("Verdana-BOLD-12"));
		label.setHorizontalAlignment(SwingConstants.CENTER);

		return label;
	}

}
