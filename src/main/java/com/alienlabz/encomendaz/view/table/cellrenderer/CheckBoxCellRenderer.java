package com.alienlabz.encomendaz.view.table.cellrenderer;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Renderizador para células de uma tabela que contenham um campo checkbox.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@SuppressWarnings("serial")
public class CheckBoxCellRenderer extends DefaultTableCellRenderer {

	public CheckBoxCellRenderer() {
		this.setHorizontalAlignment(CENTER);
	}

	@Override
	public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
		final JCheckBox check = new JCheckBox();
		check.setSelected(Boolean.valueOf(value.toString()));
		check.setHorizontalAlignment(CENTER);
		final JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(check);
		return panel;
	}

}
