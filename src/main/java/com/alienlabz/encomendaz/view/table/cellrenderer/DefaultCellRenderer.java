package com.alienlabz.encomendaz.view.table.cellrenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import org.pushingpixels.substance.api.renderers.SubstanceDefaultTableCellRenderer;

/**
 * Renderizador padrão para as celulas da tabela de rastreamentos.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class DefaultCellRenderer extends SubstanceDefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;

	@Override
	public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {

		final String code = (String) value;
		final JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		label.setText(code);
		label.setFont(Font.decode("Verdana-NORMAL-11"));
		label.setForeground(Color.DARK_GRAY);
		label.setHorizontalAlignment(SwingConstants.CENTER);

		return label;
	}

}
