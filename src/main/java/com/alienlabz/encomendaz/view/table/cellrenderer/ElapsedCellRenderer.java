package com.alienlabz.encomendaz.view.table.cellrenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import org.alfredlibrary.utilitarios.data.CalculoData;
import org.pushingpixels.substance.api.renderers.SubstanceDefaultTableCellRenderer;

import com.alienlabz.encomendaz.domain.Tracking;

/**
 * Renderizador da célula que apresentará o tempo decorrido.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class ElapsedCellRenderer extends SubstanceDefaultTableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
			final boolean hasFocus, final int row, final int column) {
		final Tracking tracking = (Tracking) value;
		final JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
				column);
		label.setFont(Font.decode("Verdana-NORMAL-11"));
		label.setForeground(Color.DARK_GRAY);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		if (tracking != null && tracking.getSent() != null && tracking.getLastStatus() != null) {
			int days = 0;
			if(tracking.isDelivered()) {
				days = CalculoData.calcularDiferencaDias(tracking.getSent(), tracking.getLastStatus().getDate());
			} else {
				days = CalculoData.calcularDiferencaDias(tracking.getSent(), new Date());
			}
			label.setText(days + " dias");
			if (tracking.getEstimative() != null && tracking.getEstimative() > 0 && days > tracking.getEstimative()) {
				label.setForeground(Color.RED);
				label.setFont(Font.decode("Verdana-BOLD-10"));
			}
		} else {
			label.setText("-");
		}
		return label;
	}

}
