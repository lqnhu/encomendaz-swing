package com.alienlabz.encomendaz.view.table;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.enterprise.util.AnnotationLiteral;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.qualifiers.DetailsRequired;
import com.alienlabz.encomendaz.bus.qualifiers.Modify;
import com.alienlabz.encomendaz.domain.Person;
import com.alienlabz.encomendaz.view.model.PersonTableModel;
import com.alienlabz.encomendaz.view.table.cellrenderer.DefaultCellRenderer;
import com.alienlabz.encomendaz.view.table.cellrenderer.IconCellRenderer;


/**
 * Tabela responsável pela exibição de {@link Person}.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
final public class JTablePeople extends JScrollPane {
	private static final long serialVersionUID = 1L;
	private JTable table;

	public JTablePeople() {
		initializeJTable();
	}

	/**
	 * Inicializando a tabela que exibirá {@link Person}.
	 */
	private void initializeJTable() {
		table = new JTable();
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setDoubleBuffered(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		final TableCellRenderer defRenderer = table.getTableHeader().getDefaultRenderer();
		table.getTableHeader().setDefaultRenderer(new DefaultTableCellRenderer() {
			private static final long serialVersionUID = 1L;

			@Override
			public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {

				final Component c = defRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

				if (c instanceof DefaultTableCellRenderer) {
					final DefaultTableCellRenderer dtcr = (DefaultTableCellRenderer) c;
					dtcr.setHorizontalAlignment(SwingConstants.CENTER);
				}
				return c;
			}
		});

		table.addMouseListener(new MouseAdapter() {

			@Override
			@SuppressWarnings("serial")
			public void mouseClicked(final MouseEvent event) {
				final PersonTableModel pModel = (PersonTableModel) table.getModel();
				final Person person = pModel.getPerson(table.getSelectedRow());
				if (event.getClickCount() == 2) {
					EventManager.fire(person, new AnnotationLiteral<Modify>() {
					});
				} else {
					if (person != null) {
						EventManager.fire(person, new AnnotationLiteral<DetailsRequired>() {
						});
					}
				}
			}

		});

		setViewportView(table);
		setAutoscrolls(true);
	}

	/**
	 * Configurar as propriedades necessárias para cada tipo de célula.
	 */
	private void initializeJTableCellProperties() {
		table.setRowHeight(50);
		table.setShowVerticalLines(false);

		table.getColumnModel().getColumn(0).setCellRenderer(new IconCellRenderer());
		table.getColumnModel().getColumn(1).setCellRenderer(new DefaultCellRenderer());
		table.getColumnModel().getColumn(2).setCellRenderer(new DefaultCellRenderer());
		table.getColumnModel().getColumn(3).setCellRenderer(new DefaultCellRenderer());

		table.getColumnModel().getColumn(0).setPreferredWidth(40);
		table.getColumnModel().getColumn(0).setMaxWidth(40);

		table.getColumnModel().getColumn(1).setPreferredWidth(200);
		table.getColumnModel().getColumn(1).setMaxWidth(200);

		table.getColumnModel().getColumn(2).setPreferredWidth(150);
		table.getColumnModel().getColumn(2).setMaxWidth(150);

		table.getTableHeader().setPreferredSize(new Dimension(0, 35));
	}

	public void setPeople(final List<Person> people) {
		table.setModel(new PersonTableModel(people));
		initializeJTableCellProperties();
	}

	public Person getSelected() {
		PersonTableModel model = (PersonTableModel) table.getModel();
		return model.getPerson(table.getSelectedRow());
	}

}
