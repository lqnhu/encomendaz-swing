package com.alienlabz.encomendaz.view.component;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;


/**
 * Campo para a inserção de números com base em um formato.
 * 
 * @author Dyorgio da Silva Nascimento
 */
public class JNumberField extends JRichTextField {

	private static final long serialVersionUID = -7506506392528621022L;

	private static final NumberFormat MONETARY_FORMAT = new DecimalFormat("#0");

	/**
	 * Preenche um StringBuilder com zeros
	 * 
	 * @param zeros
	 * @return
	 */
	private static final String makeZeros(final int zeros) {
		if (zeros >= 0) {
			final StringBuilder builder = new StringBuilder();

			for (int i = 0; i < zeros; i++) {
				builder.append('0');
			}
			return builder.toString();
		} else {
			throw new RuntimeException("Número de casas decimais inválida (" + zeros + ")");
		}
	}

	private NumberFormat numberFormat;

	private int limit = -1;

	public JNumberField() {
		this(MONETARY_FORMAT);
	}

	public JNumberField(final int casasDecimais) {
		this(new DecimalFormat((casasDecimais == 0 ? "#,##0" : "#,##0.") + makeZeros(casasDecimais)));
	}

	public JNumberField(final NumberFormat format) {
		setIcon(IconUtil.getImageIcon(ResourceBundleFactory.getImagesBundle().getString("default.numberfield.icon")));
		numberFormat = format;
		setHorizontalAlignment(RIGHT);
		setDocument(new PlainDocument() {

			private static final long serialVersionUID = 1L;

			@Override
			public void insertString(final int offs, final String str, final AttributeSet a) throws BadLocationException {
				String text = new StringBuilder(JNumberField.this.getText().replaceAll("[^0-9]", "")).append(str.replaceAll("[^0-9]", "")).toString();
				super.remove(0, getLength());
				if (text.isEmpty()) {
					text = "0";
				} else {
					text = new BigInteger(text).toString();
				}

				super.insertString(0, numberFormat.format(new BigDecimal(getLimit() > 0 && text.length() > getLimit() ? text.substring(0, getLimit()) : text)
						.divide(new BigDecimal(Math.pow(10, numberFormat.getMaximumFractionDigits())))), a);
			}

			@Override
			public void remove(final int offs, final int len) throws BadLocationException {
				super.remove(offs, len);
				if (len != getLength()) {
					insertString(0, "", null);
				}
			}
		});
		// mantem o cursor no final do campo
		addCaretListener(new CaretListener() {

			boolean update = false;

			@Override
			public void caretUpdate(final CaretEvent e) {
				if (!update) {
					update = true;
					setCaretPosition(getText().length());
					update = false;
				}
			}
		});
		// limpa o campo se apertar DELETE
		addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(final KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DELETE) {
					setText("");
				}
			}
		});
		// formato inicial
		setText("0");
		setCaretPosition(getText().length());
	}

	/**
	 * Recupera o limite do campo.
	 * 
	 * @return
	 */
	public int getLimit() {
		return limit;
	}

	/**
	 * Recupera o formatador atual do campo
	 * 
	 * @return
	 */
	public NumberFormat getNumberFormat() {
		return numberFormat;
	}

	/**
	 * Recupera um valor BigDecimal do campo
	 * 
	 * @return
	 */
	public final BigDecimal getValue() {
		return new BigDecimal(getText().replaceAll("[^0-9]", "")).divide(new BigDecimal(Math.pow(10, numberFormat.getMaximumFractionDigits())));
	}

	/**
	 * Define o limite do campo, limit < 0 para deixar livre (default) Ignora os
	 * pontos e virgulas do formato, conta somente com os números
	 * 
	 * @param limit
	 */
	public void setLimit(final int limit) {
		this.limit = limit;
	}

	/**
	 * Define o formatador do campo
	 * 
	 * @param numberFormat
	 */
	public void setNumberFormat(final NumberFormat numberFormat) {
		this.numberFormat = numberFormat;
	}

	/**
	 * Define um valor BigDecimal ao campo
	 * 
	 * @param value
	 */
	public void setValue(final Double value) {
		super.setText(numberFormat.format(value));
	}

}
