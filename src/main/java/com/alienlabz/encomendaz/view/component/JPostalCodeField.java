package com.alienlabz.encomendaz.view.component;

import java.awt.Font;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;


import org.alfredlibrary.validadores.Numeros;

import com.alienlabz.encomendaz.util.IconUtil;

/**
 * Campo que trata o recebimento apenas de valores referentes a códigos de postagem.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class JPostalCodeField extends JRichTextField {

	public JPostalCodeField(String hint) {
		super(hint);
		setIcon(IconUtil.getImageIcon("barcode.png"));
		setDocument(new JTextFieldLimit(13));
		setFont(Font.decode("Tahoma-BOLD-12"));
	}

	public class JTextFieldLimit extends PlainDocument {
		private int limit;

		JTextFieldLimit(int limit) {
			super();
			this.limit = limit;
		}

		public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
			if (str == null)
				return;

			if ((getLength() + str.length()) <= limit) {
				if (getLength() < 2 && !Numeros.isInteger(str)) {
					super.insertString(offset, str.toUpperCase(), attr);
				} else if (getLength() >= 2 && getLength() < 11 && Numeros.isInteger(str)) {
					super.insertString(offset, str.toUpperCase(), attr);
				} else if (getLength() >= 11 && !Numeros.isInteger(str)) {
					super.insertString(offset, str.toUpperCase(), attr);
				}
			}
		}
	}
}
