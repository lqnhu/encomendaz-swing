package com.alienlabz.encomendaz.view.component;

import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;

/**
 * Representa um campo que recebe apenas valores de peso.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class JWeightField extends JNumberField {
	private static final long serialVersionUID = 1L;

	public JWeightField() {
		setIcon(IconUtil.getImageIcon(ResourceBundleFactory.getImagesBundle().getString("default.weightfield.icon")));
	}

}
