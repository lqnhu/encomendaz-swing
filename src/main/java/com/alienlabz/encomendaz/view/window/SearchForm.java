package com.alienlabz.encomendaz.view.window;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.enterprise.util.AnnotationLiteral;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;

import net.miginfocom.swing.MigLayout;

import org.divxdede.swing.busy.JBusyComponent;
import org.jdesktop.swingx.JXStatusBar;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.qualifiers.PostalServiceChanged;
import com.alienlabz.encomendaz.bus.qualifiers.Searched;
import com.alienlabz.encomendaz.configuration.InternalConfiguration;
import com.alienlabz.encomendaz.domain.Person;
import com.alienlabz.encomendaz.domain.PostalService;
import com.alienlabz.encomendaz.domain.Service;
import com.alienlabz.encomendaz.domain.State;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.factory.FieldFactory;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;
import com.alienlabz.encomendaz.view.component.JPostalCodeField;
import com.alienlabz.encomendaz.view.component.JRichTextField;
import com.alienlabz.encomendaz.view.model.PersonComboBoxModel;
import com.alienlabz.encomendaz.view.model.PostalServiceComboBoxModel;
import com.alienlabz.encomendaz.view.model.ServiceComboBoxModel;
import com.alienlabz.encomendaz.view.model.StateComboBoxModel;
import com.alienlabz.encomendaz.view.table.JTableTrackings;
import com.toedter.calendar.JDateChooser;

/**
 * Janela usada para exibir uma busca detalhada de encomendas.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class SearchForm extends JDialog {
	private JComboBox postalService;
	private JComboBox service;
	private JComboBox from;
	private JComboBox to;
	private JComboBox states;
	private JPostalCodeField code;
	private JCheckBox taxed;
	private JCheckBox archived;
	private JCheckBox important;
	private JRichTextField description;
	private JDateChooser sent;
	private JBusyComponent<JPanel> busyPanel;
	private JPanel rootPanel;
	private JSplitPane mainPanel;
	private JPanel searchPanel;
	private JPanel panelFirstColumn;
	private JPanel panelSecondColumn;
	public static JLabel statusLabel = new JLabel("Informe sua pesquisa.");
	private JTableTrackings table;
	private JButton buttonBack;
	private JButton buttonSearch;
	private JButton buttonReset;
	private JXStatusBar statusBar;
	private ResourceBundle bundleMessages;
	private ResourceBundle bundleImages;
	private ResourceBundle bundleSystem;

	public SearchForm() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		bundleImages = ResourceBundleFactory.getImagesBundle();
		bundleSystem = ResourceBundleFactory.getSystemBundle();
		initialize();
		initializeFields();
		initializePanels();
		initializeFirstPanel();
		initializeSecondPanel();
		initializeToolbar();
		initializeStatusPanel();
	}

	private void initializeToolbar() {
		JToolBar toolbar = new JToolBar();
		toolbar.setPreferredSize(new Dimension(0, 50));

		buttonBack = FieldFactory.button(bundleMessages.getString("button.text.back"));
		buttonBack.setForeground(InternalConfiguration.getDefaultButtonColor());
		buttonBack.setIcon(IconUtil.getImageIcon(bundleImages.getString("button.icon.back")));

		buttonSearch = FieldFactory.button(bundleMessages.getString("button.text.search"));
		buttonSearch.setForeground(InternalConfiguration.getDefaultButtonColor());
		buttonSearch.setIcon(IconUtil.getImageIcon(bundleImages.getString("button.icon.search")));

		buttonReset = FieldFactory.button(bundleMessages.getString("button.text.reset"));
		buttonReset.setForeground(InternalConfiguration.getDefaultButtonColor());
		buttonReset.setIcon(IconUtil.getImageIcon(bundleImages.getString("button.icon.rotate")));

		toolbar.add(buttonSearch);
		toolbar.add(buttonReset);
		toolbar.add(buttonBack);

		buttonReset.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				code.setText("");
				description.setText("");
				archived.setSelected(false);
				important.setSelected(false);
				taxed.setSelected(false);
				sent.setDate(null);
				service.setSelectedItem(null);
				postalService.setSelectedItem(null);
				from.setSelectedItem(null);
				to.setSelectedItem(null);
				states.setSelectedItem(null);
				table.setTrackings(new ArrayList<Tracking>());
			}

		});

		buttonSearch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EventManager.fire(SearchForm.this, new AnnotationLiteral<Searched>() {
				});
			}

		});

		buttonBack.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				SearchForm.this.dispose();
			}

		});

		rootPanel.add(toolbar, BorderLayout.NORTH);
	}

	public void setSent(final Date date) {
		sent.setDate(date);
	}

	public void setStatesList(List<State> mStates) {
		final ComboBoxModel oldModel = states.getModel();
		final State selected = (State) oldModel.getSelectedItem();
		final StateComboBoxModel model = new StateComboBoxModel();
		model.setStates(mStates);
		states.setModel(model);
		if (selected != null) {
			states.getModel().setSelectedItem(selected);
		}
	}

	public void setCode(final String code) {
		this.code.setText(code);
	}

	public void setTaxed(final boolean taxed) {
		this.taxed.setSelected(taxed);
	}

	public void setImportant(final boolean important) {
		this.important.setSelected(important);
	}

	public void setDescription(final String description) {
		this.description.setText(description);
	}

	public void setPostalServicesList(List<PostalService> list) {
		final ComboBoxModel oldModel = postalService.getModel();
		final Service selected = (Service) oldModel.getSelectedItem();
		final PostalServiceComboBoxModel model = new PostalServiceComboBoxModel();
		model.setPostalServices(list);
		postalService.setModel(model);
		if (selected != null) {
			postalService.getModel().setSelectedItem(selected);
		}
	}

	public void setServicesList(List<Service> list) {
		final ServiceComboBoxModel model = new ServiceComboBoxModel();
		model.setServices(list);
		service.setModel(model);
		service.repaint();
	}

	public void setFromList(final List<Person> people) {
		final ComboBoxModel oldModel = from.getModel();
		final Person selected = (Person) oldModel.getSelectedItem();
		final PersonComboBoxModel model = new PersonComboBoxModel();
		model.setPeople(people);
		from.setModel(model);
		if (selected != null) {
			from.getModel().setSelectedItem(selected);
		}
	}

	public void setToList(final List<Person> people) {
		final ComboBoxModel oldModel = to.getModel();
		final Person selected = (Person) oldModel.getSelectedItem();
		final PersonComboBoxModel model = new PersonComboBoxModel();
		model.setPeople(people);
		to.setModel(model);
		if (selected != null) {
			to.getModel().setSelectedItem(selected);
		}
	}

	public String getCode() {
		return code.getText();
	}

	public String getDescription() {
		return description.getText();
	}

	public State getState() {
		return (State) states.getSelectedItem();
	}

	public PostalService getPostalService() {
		return (PostalService) postalService.getSelectedItem();
	}

	public boolean isArchived() {
		return archived.isSelected();
	}

	public Service getService() {
		return (Service) service.getSelectedItem();
	}

	public Person getFrom() {
		return (Person) from.getSelectedItem();
	}

	public Date getSent() {
		return sent.getDate();
	}

	public boolean isTaxed() {
		return taxed.isSelected();
	}

	public boolean isImportant() {
		return important.isSelected();
	}

	public Person getTo() {
		return (Person) to.getSelectedItem();
	}

	private void initializePanels() {
		rootPanel = new JPanel(new BorderLayout());
		mainPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		searchPanel = new JPanel(new GridLayout(1, 2));
		panelFirstColumn = new JPanel(new MigLayout("fillx", "[align right][grow]"));
		panelSecondColumn = new JPanel(new MigLayout("fillx", "[align right][grow]"));
		table = new JTableTrackings();

		mainPanel.setDividerLocation(190);
		mainPanel.setOneTouchExpandable(true);

		searchPanel.add(panelFirstColumn);
		searchPanel.add(panelSecondColumn);

		mainPanel.add(searchPanel);
		mainPanel.add(table);

		rootPanel.add(mainPanel);

		busyPanel = new JBusyComponent<JPanel>(rootPanel);
		add(busyPanel);
	}

	public void setBusy(boolean b) {
		busyPanel.setBusy(b);
	}

	private void initializeSecondPanel() {
		panelSecondColumn.add(new JLabel(bundleMessages.getString("form.search.label.code")), "gap 10");
		panelSecondColumn.add(code, "span, growx");

		panelSecondColumn.add(new JLabel(bundleMessages.getString("form.search.label.description")), "gap 10");
		panelSecondColumn.add(description, "span, growx");

		panelSecondColumn.add(new JLabel(bundleMessages.getString("form.search.label.sent")), "gap 10");
		panelSecondColumn.add(sent, "span, growx");

		panelSecondColumn.add(new JLabel(bundleMessages.getString("form.search.label.taxed")), "gap 10");
		panelSecondColumn.add(taxed, "span, growx");

		panelSecondColumn.add(new JLabel(bundleMessages.getString("form.search.label.importants")), "gap 10");
		panelSecondColumn.add(important, "span, growx");

		panelSecondColumn.add(new JLabel(bundleMessages.getString("form.search.label.archived")), "gap 10");
		panelSecondColumn.add(archived, "span, growx");
	}

	private void initializeFirstPanel() {

		panelFirstColumn.add(new JLabel(bundleMessages.getString("form.search.label.state")), "gap 10");
		panelFirstColumn.add(states, "span, growx");

		panelFirstColumn.add(new JLabel(bundleMessages.getString("form.search.label.postalservice")), "gap 10");
		panelFirstColumn.add(postalService, "span, growx");

		panelFirstColumn.add(new JLabel(bundleMessages.getString("form.search.label.service")), "gap 10");
		panelFirstColumn.add(service, "span, growx");

		panelFirstColumn.add(new JLabel(bundleMessages.getString("form.search.label.from")), "gap 10");
		panelFirstColumn.add(from, "span, growx");

		panelFirstColumn.add(new JLabel(bundleMessages.getString("form.search.label.to")), "gap 10");
		panelFirstColumn.add(to, "span, growx");
	}


	private void initializeFields() {
		postalService = FieldFactory.combo();
		service = FieldFactory.combo();
		from = FieldFactory.combo();
		to = FieldFactory.combo();
		states = FieldFactory.combo();
		code = FieldFactory.postalCode("");
		taxed = FieldFactory.checkbox();
		archived = FieldFactory.checkbox();
		important = FieldFactory.checkbox();
		description = FieldFactory.richtext("");
		sent = FieldFactory.date();
		postalService.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent event) {
				EventManager.fire(SearchForm.this, new AnnotationLiteral<PostalServiceChanged>() {
				});
			}

		});
	}

	private void initialize() {
		setPreferredSize(InternalConfiguration.getSearchFormDimension());
		setResizable(true);
		setModal(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	public void setResult(List<Tracking> search) {
		table.setTrackings(search);
		setStatusText("Sua pesquisa retornou " + search.size() + " rastreamento(s).");
	}

	public void setStatusText(String text) {
		statusLabel.setText(text);
	}

	/**
	 * Inicializar o painel de status da aplicação.
	 */
	private void initializeStatusPanel() {
		statusBar = new JXStatusBar();
		statusBar.setSize(new Dimension(0, 35));
		statusBar.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 0, Color.LIGHT_GRAY));

		statusLabel.setFont(Font.decode(bundleSystem.getString("statusbar.search.label.font")));

		final JXStatusBar.Constraint c1 = new JXStatusBar.Constraint(JXStatusBar.Constraint.ResizeBehavior.FILL);
		c1.setFixedWidth(100);
		statusBar.add(statusLabel, c1);

		add(statusBar, BorderLayout.SOUTH);
	}
}
