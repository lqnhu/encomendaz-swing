package com.alienlabz.encomendaz.view.window;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.alienlabz.encomendaz.domain.TrackingStatus;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.StringUtil;
import com.alienlabz.encomendaz.view.table.JTableStatus;

import net.miginfocom.swing.MigLayout;

/**
 * Painel em abas que exibe as informações de um rastreamento.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class DetailsTabPane extends JTabbedPane {
	private static final long serialVersionUID = 1L;
	private JPanel panelStatus = new JPanel(new BorderLayout());
	private JPanel panelDetailsOverall = new JPanel(new GridLayout(1, 2));
	private JPanel panelDetailsFirstColumn = new JPanel(new MigLayout("fillx", "[align right][grow]"));
	private JPanel panelDetailsSecondColumn = new JPanel(new MigLayout("fillx", "[align right][grow]"));
	private JTextField from = new JTextField();
	private JTextField to = new JTextField();
	private JTextField estimative = new JTextField();
	private JTextField code = new JTextField();
	private JTextField lastStatus = new JTextField();
	private JTextField description = new JTextField();
	private JTableStatus tableStatus;
	private final ResourceBundle messages;

	public DetailsTabPane() {
		messages = ResourceBundleFactory.getMessagesBundle();

		addTab(messages.getString("tab.tracking.statuses.label"), panelStatus);
		addTab(messages.getString("tab.tracking.details.label"), panelDetailsOverall);

		initializeTableStatus();
		initializeDetailsPanelFirstColumn();
		initializeDetailsPanelSecondColumn();
	}

	private void initializeDetailsPanelSecondColumn() {
		panelDetailsSecondColumn.setBorder(new TitledBorder("Quem?"));

		from.setEnabled(false);
		panelDetailsSecondColumn.add(new JLabel(messages.getString("form.entrance.details.from")), "gap 10");
		panelDetailsSecondColumn.add(from, "span, growx");

		to.setEnabled(false);
		panelDetailsSecondColumn.add(new JLabel(messages.getString("form.entrance.details.to")), "gap 10");
		panelDetailsSecondColumn.add(to, "span, growx");

		estimative.setEnabled(false);
		panelDetailsSecondColumn.add(new JLabel(messages.getString("form.entrance.details.estimative")), "gap 10");
		panelDetailsSecondColumn.add(estimative, "span, growx");

		panelDetailsOverall.add(panelDetailsSecondColumn);
	}

	/**
	 * Iniciar a criação do painel que apresentará os detalhes da encomenda.
	 */
	private void initializeDetailsPanelFirstColumn() {
		panelDetailsFirstColumn.setBorder(new TitledBorder("Geral"));

		code.setEnabled(false);
		panelDetailsFirstColumn.add(new JLabel(messages.getString("form.entrance.details.code")), "gap 10");
		panelDetailsFirstColumn.add(code, "span, growx");

		lastStatus.setEnabled(false);
		panelDetailsFirstColumn.add(new JLabel(messages.getString("form.entrance.details.laststatus")), "gap 10");
		panelDetailsFirstColumn.add(lastStatus, "span, growx");

		description.setEnabled(false);
		panelDetailsFirstColumn.add(new JLabel(messages.getString("form.entrance.details.description")), "gap 10");
		panelDetailsFirstColumn.add(description, "span, growx");

		panelDetailsOverall.add(panelDetailsFirstColumn);
	}

	public JPanel getPanelDetails() {
		return panelDetailsFirstColumn;
	}

	public JPanel getPanelStatus() {
		return panelStatus;
	}

	/**
	 * Inicializar a tabela que apresenta as situações.
	 */
	private void initializeTableStatus() {
		tableStatus = new JTableStatus();
		panelStatus.add(tableStatus);
	}

	public void setPanelDetails(final JPanel panelDetails) {
		this.panelDetailsFirstColumn = panelDetails;
	}

	public void setPanelStatus(final JPanel panelStatus) {
		this.panelStatus = panelStatus;
	}

	/**
	 * Definir a lista de situações a exibir na aba de situações.
	 * 
	 * @param statuses Lista de situações.
	 */
	public void setStatuses(final List<TrackingStatus> statuses) {
		tableStatus.setStatuses(statuses);
	}

	public void setCode(String code) {
		this.code.setText(code);
	}

	public void setLastStatus(String s) {
		if (s != null && !StringUtil.isNullOrEmpty(s)) {
			this.lastStatus.setText(s);
		} else {
			this.lastStatus.setText(messages.getString("form.entrance.details.labels.notinformed"));
		}
	}

	public void setDescription(String s) {
		if (s != null && !StringUtil.isNullOrEmpty(s)) {
			this.description.setText(s);
		} else {
			this.description.setText(messages.getString("form.entrance.details.labels.notinformed"));
		}
	}

	public void setFromDetails(String s) {
		if (s != null && !StringUtil.isNullOrEmpty(s)) {
			this.from.setText(s);
		} else {
			this.from.setText(messages.getString("form.entrance.details.labels.notinformed"));
		}
	}

	public void setToDetails(String s) {
		if (s != null && !StringUtil.isNullOrEmpty(s)) {
			this.to.setText(s);
		} else {
			this.to.setText(messages.getString("form.entrance.details.labels.notinformed"));
		}
	}

	public void setEstimative(String s) {
		if (s != null && !StringUtil.isNullOrEmpty(s)) {
			this.estimative.setText(s);
		} else {
			this.estimative.setText(messages.getString("form.entrance.details.labels.notinformed"));
		}
	}

}
