package com.alienlabz.encomendaz.view.window;

/**
 * Define o comportamento padrão para formulários de operações básicas de CRUD.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public interface CrudForm {

	void close();

	void disabledDeleteButton();

	void disableSaveButton();

	void enableDeleteButton();

	void enableSaveButton();

	boolean isValid();

}
