package com.alienlabz.encomendaz.view.window;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.border.TitledBorder;

import net.miginfocom.swing.MigLayout;

import org.divxdede.swing.busy.JBusyComponent;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.events.BatchCodesInsertion;
import com.alienlabz.encomendaz.configuration.InternalConfiguration;
import com.alienlabz.encomendaz.factory.FieldFactory;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;

/**
 * Formulário para cadastro de encomendas em lote.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class BatchForm extends JDialog {
	private ResourceBundle bundleMessages;
	private ResourceBundle bundleImages;
	private JTextArea codes;
	private JComboBox separator;
	private JScrollPane scroll;
	private Map<String, String> separatorOptions;
	private JToolBar toolbar;
	private JPanel mainPanel;
	private JBusyComponent<JPanel> busy;

	public BatchForm() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		bundleImages = ResourceBundleFactory.getImagesBundle();
		initialize();
		initializeCodesPanel();
		initializeToolbar();
	}

	public void setBusy(boolean busy) {
		this.busy.setBusy(busy);
	}

	/**
	 * Criar os botões da toolbar.
	 */
	private void initializeToolbar() {
		toolbar = new JToolBar();
		toolbar.setPreferredSize(new Dimension(0, 50));
		JButton buttonImport = new JButton(bundleMessages.getString("batch.import.label"));
		buttonImport.setIcon(IconUtil.getImageIcon(bundleImages.getString("generic.import.icon")));
		buttonImport.setForeground(InternalConfiguration.getDefaultButtonColor());
		toolbar.add(buttonImport);
		buttonImport.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				EventManager.fire(new BatchCodesInsertion(codes.getText(), (String) separatorOptions.get(separator.getSelectedItem())));
			}

		});

		JButton buttonBack = new JButton(bundleMessages.getString("button.text.back"));
		buttonBack.setForeground(InternalConfiguration.getDefaultButtonColor());
		buttonBack.setIcon(IconUtil.getImageIcon(bundleImages.getString("button.icon.back")));
		toolbar.add(buttonBack);
		buttonBack.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent arg0) {
				BatchForm.this.setVisible(false);
			}

		});

		add(toolbar, BorderLayout.NORTH);
	}

	/**
	 * Inicializar o painel que vai exibir um textarea para o usuário colar os códigos.
	 */
	private void initializeCodesPanel() {
		separatorOptions = new HashMap<String, String>();
		separatorOptions.put(bundleMessages.getString("batch.separator.option.comma"), ",");
		separatorOptions.put(bundleMessages.getString("batch.separator.option.dot"), ".");
		separatorOptions.put(bundleMessages.getString("batch.separator.option.colon"), ":");
		separatorOptions.put(bundleMessages.getString("batch.separator.option.semicolon"), ";");
		separatorOptions.put(bundleMessages.getString("batch.separator.option.newline"), "\n");

		JPanel panel = new JPanel(new MigLayout("fillx", "[align right][grow]"));
		panel.setBorder(new TitledBorder(bundleMessages.getString("batch.form.panel.codes.title")));

		panel.add(new JLabel(bundleMessages.getString("dialog.batch.separator.label")), "gap 10");
		separator = FieldFactory.combo();
		separator.setModel(new DefaultComboBoxModel(separatorOptions.keySet().toArray()));
		panel.add(separator, "span, growx");

		codes = FieldFactory.textarea();
		codes.setRows(10);
		scroll = new JScrollPane(codes);
		scroll.setAutoscrolls(true);
		panel.add(scroll, "span, growx");
		mainPanel.add(panel, "span, growx");
	}

	/**
	 * Inicializar propriedades da janela
	 */
	private void initialize() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setModal(true);
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(550, 350));
		setTitle(bundleMessages.getString("batch.form.title"));
		mainPanel = new JPanel(new MigLayout("fillx", "[align left]"));
		busy = new JBusyComponent<JPanel>(mainPanel);
		add(busy, BorderLayout.CENTER);
	}

}
