package com.alienlabz.encomendaz.view.window;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ResourceBundle;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;

/**
 * Janela para apresentar informações sobre o programa.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class AboutForm extends JDialog {
	private ResourceBundle bundleMessages;

	public AboutForm() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		initialize();
	}

	private void initialize() {
		setLayout(new BorderLayout());
		setModal(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle(bundleMessages.getString("form.title.about"));
		setPreferredSize(new Dimension(502, 316));
		JLabel image = new JLabel();
		image.setHorizontalAlignment(SwingConstants.CENTER);
		image.setIcon(IconUtil.getImageIcon("about.png", "images"));
		add(image);
	}

}
