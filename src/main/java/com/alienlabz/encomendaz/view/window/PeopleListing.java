package com.alienlabz.encomendaz.view.window;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ResourceBundle;

import javax.enterprise.util.AnnotationLiteral;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JToolBar;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.qualifiers.Deleted;
import com.alienlabz.encomendaz.bus.qualifiers.NewRequired;
import com.alienlabz.encomendaz.configuration.InternalConfiguration;
import com.alienlabz.encomendaz.domain.Person;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;
import com.alienlabz.encomendaz.view.table.JTablePeople;


/**
 * Tela de Listagem de Pessoas ({@link Person}).
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
final public class PeopleListing extends JDialog {
	private static final long serialVersionUID = 1L;
	private JTablePeople table;
	private JToolBar toolbar;
	private ResourceBundle bundleMessages;
	private ResourceBundle bundleImages;

	public PeopleListing() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		bundleImages = ResourceBundleFactory.getImagesBundle();
		initialize();
		initializeJTable();
		initializeToolbar();
	}

	/**
	 * Inicializar a barra de ações.
	 */
	private void initializeToolbar() {
		toolbar = new JToolBar();
		JButton buttonAdd = new JButton(bundleMessages.getString("add.user.label"));
		buttonAdd.setIcon(IconUtil.getImageIcon(bundleImages.getString("icon.add.user")));
		buttonAdd.setForeground(InternalConfiguration.getDefaultButtonColor());
		toolbar.add(buttonAdd);
		buttonAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				EventManager.fire(new Person(), new AnnotationLiteral<NewRequired>() {
				});
			}

		});

		JButton buttonDelete = new JButton(bundleMessages.getString("delete.user.label"));
		buttonDelete.setIcon(IconUtil.getImageIcon(bundleImages.getString("icon.delete.user")));
		buttonDelete.setForeground(InternalConfiguration.getDefaultButtonColor());
		buttonDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (table.getSelected() != null) {
					EventManager.fire(table.getSelected(), new AnnotationLiteral<Deleted>() {
					});
				}
			}

		});
		toolbar.add(buttonDelete);

		JButton buttonBack = new JButton(bundleMessages.getString("button.text.back"));
		buttonBack.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent arg0) {
				PeopleListing.this.setVisible(false);
			}

		});

		buttonBack.setIcon(IconUtil.getImageIcon(bundleImages.getString("button.icon.back")));
		buttonBack.setForeground(InternalConfiguration.getDefaultButtonColor());
		toolbar.add(buttonBack);

		add(toolbar, BorderLayout.NORTH);
	}

	/**
	 * Inicializar a tabela que exibirá a lista de pessoas.
	 */
	private void initializeJTable() {
		table = new JTablePeople();
		add(table, BorderLayout.CENTER);
	}

	/**
	 * Inicializar os atributos da janela.
	 */
	private void initialize() {
		setModal(true);
		setResizable(true);
		setPreferredSize(new Dimension(600, 400));
		setLayout(new BorderLayout());
		setTitle(bundleMessages.getString("people.list.title"));
	}

	/**
	 * Definir as pessoas que aparecerão na listagem.
	 * 
	 * @param people Pessoas que aparecerão na listagem.
	 */
	public void setPeople(List<Person> people) {
		table.setPeople(people);
	}

}
