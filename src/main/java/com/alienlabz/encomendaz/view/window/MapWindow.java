package com.alienlabz.encomendaz.view.window;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JDialog;
import javax.swing.JPanel;


import org.divxdede.swing.busy.JBusyComponent;
import org.jdesktop.swingx.JXMapKit;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.Waypoint;
import org.jdesktop.swingx.painter.Painter;

import com.alienlabz.encomendaz.configuration.InternalConfiguration;

/**
 * Janela usada para exibir um Mapa.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class MapWindow extends JDialog {
	private JXMapKit mapKit;
	private JPanel panelMap = new JPanel(new BorderLayout());
	private JBusyComponent<JPanel> busyPanelMap;

	public MapWindow() {
		setLayout(new BorderLayout());
		setPreferredSize(InternalConfiguration.getMapWindowDimension());
		setResizable(true);
		setModal(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		initialize();
	}

	public void setBusy(boolean busy) {
		busyPanelMap.setBusy(busy);
	}

	public void setMapWayPoints(final List<double[]> points) {
		if (points == null || points.size() == 0) {
			return;
		}

		Set<Waypoint> waypoints = new HashSet<Waypoint>();
		GeoPosition geoPosition = null;
		for (double[] point : points) {
			waypoints.add(new Waypoint(point[0], point[1]));
			if (geoPosition == null) {
				geoPosition = new GeoPosition(point[0], point[1]);
			}
		}
//		WaypointPainter painter = new WaypointPainter();
//		painter.setWaypoints(waypoints);
//		mapKit.getMainMap().setOverlayPainter(painter);

		mapKit.setVisible(true);
		mapKit.repaint();

		if (geoPosition != null) {
			mapKit.setCenterPosition(geoPosition);
		}

		Painter<JXMapViewer> lineOverlay = new Painter<JXMapViewer>() {

			public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
				g = (Graphics2D) g.create();
				Rectangle rect = mapKit.getMainMap().getViewportBounds();
				g.translate(-rect.x, -rect.y);

				g.setColor(Color.RED);
				g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				g.setStroke(new BasicStroke(2));

				int lastX = -1;
				int lastY = -1;
				for (double[] point : points) {
					GeoPosition gp = new GeoPosition(point[0], point[1]);
					Point2D pt = mapKit.getMainMap().getTileFactory().geoToPixel(gp, mapKit.getMainMap().getZoom());
					if (lastX != -1 && lastY != -1) {
						g.drawLine(lastX, lastY, (int) pt.getX(), (int) pt.getY());
					}
					lastX = (int) pt.getX();
					lastY = (int) pt.getY();
				}

				g.dispose();
			}
		};
		mapKit.getMainMap().setOverlayPainter(lineOverlay);
		mapKit.repaint();
	}

	private void initialize() {
		mapKit = new JXMapKit();
		mapKit.setDefaultProvider(JXMapKit.DefaultProviders.OpenStreetMaps);

		//		WMSService wms = new WMSService();
		//		wms.setLayer("BMNG");
		//		wms.setBaseUrl("http://wms.jpl.nasa.gov/wms.cgi?");
		//		TileFactory fact = new WMSTileFactory(wms);

		//		map.setTileFactory(fact);
		panelMap.add(mapKit);

		busyPanelMap = new JBusyComponent<JPanel>(panelMap);
		add(busyPanelMap);
	}

}
