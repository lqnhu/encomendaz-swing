package com.alienlabz.encomendaz.view.window;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.List;

import javax.enterprise.util.AnnotationLiteral;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;

import org.divxdede.swing.busy.JBusyComponent;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.qualifiers.CalculatePrice;
import com.alienlabz.encomendaz.bus.qualifiers.CodeFieldLostFocus;
import com.alienlabz.encomendaz.bus.qualifiers.FromComboItemChanged;
import com.alienlabz.encomendaz.bus.qualifiers.PostalServiceChanged;
import com.alienlabz.encomendaz.bus.qualifiers.ToComboItemChanged;
import com.alienlabz.encomendaz.bus.qualifiers.WindowClosed;
import com.alienlabz.encomendaz.configuration.InternalConfiguration;
import com.alienlabz.encomendaz.domain.Person;
import com.alienlabz.encomendaz.domain.PostalService;
import com.alienlabz.encomendaz.domain.Service;
import com.alienlabz.encomendaz.factory.FieldFactory;
import com.alienlabz.encomendaz.util.NumberUtil;
import com.alienlabz.encomendaz.util.SwingUtil;
import com.alienlabz.encomendaz.view.component.JMoneyField;
import com.alienlabz.encomendaz.view.component.JNumberField;
import com.alienlabz.encomendaz.view.component.JPostalCodeField;
import com.alienlabz.encomendaz.view.component.JRichTextField;
import com.alienlabz.encomendaz.view.component.JWeightField;
import com.alienlabz.encomendaz.view.model.PersonComboBoxModel;
import com.alienlabz.encomendaz.view.model.PostalServiceComboBoxModel;
import com.alienlabz.encomendaz.view.model.ServiceComboBoxModel;
import com.alienlabz.encomendaz.view.renderer.PostalServicesComboRenderer;
import com.toedter.calendar.JDateChooser;

/**
 * Formulário de Cadastro de Rastreamentos.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class TrackingForm extends GenericCrudForm {
	private static final long serialVersionUID = 1L;
	private JTabbedPane tabs;
	private JButton buttonCalc;
	private JBusyComponent<JTextField> busyValue;
	private JPostalCodeField code;
	private JRichTextField description;
	private JTextArea details;
	private JNumberField estimate;
	private JNumberField height;
	private JNumberField width;
	private JNumberField girth;
	private JNumberField length;
	private JMoneyField value;
	private JMoneyField declaredValue;
	private JWeightField weight;
	private JDateChooser sentDate;
	private JPanel panelForm;
	private JComboBox from;
	private JComboBox to;
	private JComboBox postalServices;
	private JComboBox services;
	private JCheckBox delivered;
	private JCheckBox archived;
	private JCheckBox notifyFromByMail;
	private JCheckBox notifyFromBySMS;
	private JCheckBox notifyFromByTwitter;
	private JCheckBox notifyToByMail;
	private JCheckBox notifyToBySMS;
	private JCheckBox notifyToByTwitter;
	private JCheckBox important;
	private JPanel panelNotifications;

	public TrackingForm() {
		initializeTabbedPane();
		initializeFormProperties();
		initializePanelForm();
		initializePanelPackage();
		initializePanelNotifications();
		initializeDetailsTab();
		initializeButtons();

		add(tabs, BorderLayout.CENTER);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(final WindowEvent e) {
				code.requestFocus();
			}

			@Override
			public void windowClosed(WindowEvent e) {
				EventManager.fire(TrackingForm.this, new AnnotationLiteral<WindowClosed>(){});
			}

		});
	}

	/**
	 * Criar a aba para apresentar as características do pacote.
	 */
	private void initializePanelPackage() {
		JPanel panelPackage = new JPanel(new MigLayout("fillx", "[align right][grow]"));

		length = FieldFactory.number();
		height = FieldFactory.number();
		width = FieldFactory.number();
		girth = FieldFactory.number();

		panelPackage.add(new JLabel(bundleMessages.getString("dialog.tracking.form.width.label")), "gap 10");
		panelPackage.add(width, "span, growx");
		panelPackage.add(new JLabel(bundleMessages.getString("dialog.tracking.form.height.label")), "gap 10");
		panelPackage.add(height, "span, growx");
		panelPackage.add(new JLabel(bundleMessages.getString("dialog.tracking.form.length.label")), "gap 10");
		panelPackage.add(length, "span, growx");
		panelPackage.add(new JLabel(bundleMessages.getString("dialog.tracking.form.girth.label")), "gap 10");
		panelPackage.add(girth, "span, growx");
		panelPackage.add(new JLabel(bundleMessages.getString("dialog.tracking.form.weight.label")), "gap 10");
		panelPackage.add(weight, "span, growx");

		tabs.addTab(bundleMessages.getString("dialog.tracking.form.tab.package.title"), panelPackage);
	}

	public String getCode() {
		return code.getText();
	}

	public String getDescription() {
		return description.getText();
	}

	public String getDetails() {
		return details.getText();
	}

	public Double getEstimative() {
		return NumberUtil.toDouble(estimate.getText());
	}

	public Person getFrom() {
		return (Person) from.getSelectedItem();
	}

	public Double getDeclaredValue() {
		return NumberUtil.convertCurrency(declaredValue.getText());
	}

	public Date getSent() {
		return sentDate.getDate();
	}

	public Person getTo() {
		return (Person) to.getSelectedItem();
	}

	public Double getValue() {
		return NumberUtil.convertCurrency(value.getText());
	}

	public Double getWeight() {
		return NumberUtil.toDouble(weight.getText());
	}

	/**
	 * Inicializar a aba que contém somente o campo para detalhes do rastreamento.
	 */
	private void initializeDetailsTab() {
		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(details);
		tabs.addTab(bundleMessages.getString("dialog.tracking.form.tab.details.title"), panel);
	}

	/**
	 * Apenas instanciar todos os campos que formam o formulário.
	 */
	private void initializeFields() {
		code = FieldFactory.postalCode(bundleMessages.getString("dialog.tracking.form.code.hint"));
		code.addFocusListener(new FocusAdapter() {

			@Override
			public void focusLost(final FocusEvent event) {
				EventManager.fire(TrackingForm.this, new AnnotationLiteral<CodeFieldLostFocus>(){});
			}

		});

		description = FieldFactory.richtext(bundleMessages.getString("dialog.tracking.form.description.hint"));
		sentDate = FieldFactory.date();
		value = FieldFactory.money();
		declaredValue = FieldFactory.money();
		weight = FieldFactory.weight();
		archived = FieldFactory.checkbox();
		from = FieldFactory.combo();
		from.setEditable(true);
		to = FieldFactory.combo();
		to.setEditable(true);
		postalServices = FieldFactory.combo();
		PostalServicesComboRenderer renderer = new PostalServicesComboRenderer(postalServices);
		postalServices.setRenderer(renderer);
		busyValue = new JBusyComponent<JTextField>(value);
		buttonCalc = FieldFactory.button(bundleMessages.getString("generic.button.calc"));

		buttonCalc.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				EventManager.fire(TrackingForm.this, new AnnotationLiteral<CalculatePrice>() {
				});

			}

		});
		postalServices.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent event) {
				EventManager.fire(TrackingForm.this, new AnnotationLiteral<PostalServiceChanged>(){});
			}

		});
		services = FieldFactory.combo();
		details = FieldFactory.textarea();
		delivered = FieldFactory.checkbox();
		notifyFromByMail = FieldFactory.checkbox();
		notifyFromBySMS = FieldFactory.checkbox();
		notifyFromByTwitter = FieldFactory.checkbox();
		notifyToByMail = FieldFactory.checkbox();
		notifyToBySMS = FieldFactory.checkbox();
		notifyToByTwitter = FieldFactory.checkbox();
		important = FieldFactory.checkbox();
		estimate = FieldFactory.number();
		SwingUtil.setToolTip(bundleMessages.getString("dialog.tracking.form.value.tip"), value);
		initializePersonListeners();
	}

	/**
	 * Definir as propriedades de cada campo do formulário.
	 */
	private void initializeFieldsProperties() {
		panelForm.add(new JLabel(bundleMessages.getString("dialog.tracking.form.postalservices.label")), "gap 10");
		panelForm.add(postalServices, "span, growx");
		panelForm.add(new JLabel(bundleMessages.getString("dialog.tracking.form.services.label")), "gap 10");
		panelForm.add(services, "span, growx");
		panelForm.add(new JLabel(bundleMessages.getString("dialog.tracking.form.code.label")), "gap 10");
		panelForm.add(code, "span, growx");
		panelForm.add(new JLabel(bundleMessages.getString("dialog.tracking.form.sentdate.label")), "gap 10");
		panelForm.add(sentDate, "span, growx");
		panelForm.add(new JLabel(bundleMessages.getString("dialog.tracking.form.description.label")), "gap 10");
		panelForm.add(description, "span, growx");
		panelForm.add(new JLabel(bundleMessages.getString("dialog.tracking.form.insurance.label")), "gap 10");
		panelForm.add(declaredValue, "span, growx");
		panelForm.add(new JLabel(bundleMessages.getString("dialog.tracking.form.from.label")), "gap 10");
		panelForm.add(from, "span, growx");
		panelForm.add(new JLabel(bundleMessages.getString("dialog.tracking.form.to.label")), "gap 10");
		panelForm.add(to, "span, growx");
		panelForm.add(new JLabel(bundleMessages.getString("dialog.tracking.form.estimate.label")), "gap 10");
		panelForm.add(estimate, "span, growx");
		panelForm.add(new JLabel(bundleMessages.getString("dialog.tracking.form.value.label")), "gap 10");
		panelForm.add(busyValue, "growx");
		panelForm.add(buttonCalc, "span, growx");
		panelForm.add(new JLabel(bundleMessages.getString("dialog.tracking.form.archived.label")), "gap 10");
		panelForm.add(archived, "span, growx");
		panelForm.add(new JLabel(bundleMessages.getString("dialog.tracking.form.delivered.label")), "gap 10");
		panelForm.add(delivered, "span, growx");
		panelForm.add(new JLabel(bundleMessages.getString("dialog.tracking.form.important.label")), "gap 10");
		panelForm.add(important, "span, growx");
	}

	public void setCalculatePriceBusy(boolean busy) {
		busyValue.setBusy(busy);
		buttonCalc.setEnabled(!busy);
	}

	/**
	 * Definir as propriedades do próprio formulário.
	 */
	private void initializeFormProperties() {
		setLayout(new BorderLayout());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setPreferredSize(InternalConfiguration.getTrackingFormDimension());
		setResizable(true);
		setModal(true);
		setTitle(bundleMessages.getString("form.tracking.title"));
	}

	/**
	 * Criar o painel que conterá o formulário.
	 */
	private void initializePanelForm() {
		panelForm = new JPanel(new MigLayout("fillx", "[align right][grow]"));
		initializeFields();
		initializeFieldsProperties();
		tabs.addTab(bundleMessages.getString("dialog.tracking.form.tab.tracking.title"), panelForm);
	}

	/**
	 * Inicializar o painel de notificações.
	 */
	private void initializePanelNotifications() {
		panelNotifications = new JPanel(new MigLayout("fillx", "[align right][grow]"));
		panelNotifications.add(
				new JLabel(bundleMessages.getString("dialog.tracking.form.notifications.separator.from.title")),
				"split, span, gaptop 10");
		panelNotifications.add(new JSeparator(), "growx, wrap, gaptop 10");
		panelNotifications.add(new JLabel(bundleMessages.getString("dialog.tracking.form.notifybymail.label")),
				"gap 10");
		panelNotifications.add(notifyFromByMail, "span, growx");
		panelNotifications
				.add(new JLabel(bundleMessages.getString("dialog.tracking.form.notifybysms.label")), "gap 10");
		panelNotifications.add(notifyFromBySMS, "span, growx");
		panelNotifications.add(new JLabel(bundleMessages.getString("dialog.tracking.form.notifybytwitter.label")),
				"gap 10");
		panelNotifications.add(notifyFromByTwitter, "span, growx");
		panelNotifications.add(
				new JLabel(bundleMessages.getString("dialog.tracking.form.notifications.separator.to.title")),
				"split, span, gaptop 10");
		panelNotifications.add(new JSeparator(), "growx, wrap, gaptop 10");
		panelNotifications.add(new JLabel(bundleMessages.getString("dialog.tracking.form.notifybymail.label")),
				"gap 10");
		panelNotifications.add(notifyToByMail, "span, growx");
		panelNotifications
				.add(new JLabel(bundleMessages.getString("dialog.tracking.form.notifybysms.label")), "gap 10");
		panelNotifications.add(notifyToBySMS, "span, growx");
		panelNotifications.add(new JLabel(bundleMessages.getString("dialog.tracking.form.notifybytwitter.label")),
				"gap 10");
		panelNotifications.add(notifyToByTwitter, "span, growx");
		tabs.addTab(bundleMessages.getString("dialog.tracking.form.tab.notifications.title"), panelNotifications);
	}

	/**
	 * Inicializar os listeners para os combos de pessoas.
	 */
	private void initializePersonListeners() {
		from.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent arg0) {
				EventManager.fire(TrackingForm.this, new AnnotationLiteral<FromComboItemChanged>(){});
			}

		});

		to.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				EventManager.fire(TrackingForm.this, new AnnotationLiteral<ToComboItemChanged>(){});
			}

		});
	}

	/**
	 * Inicializar o painel de abas.
	 */
	private void initializeTabbedPane() {
		tabs = new JTabbedPane();
	}

	public boolean isDelivered() {
		return delivered.isSelected();
	}

	public boolean isImportant() {
		return important.isSelected();
	}

	public boolean isNotifyFromByMail() {
		return notifyFromByMail.isSelected();
	}

	public boolean isNotifyFromBySMS() {
		return notifyFromBySMS.isSelected();
	}

	public boolean isNotifyFromByTwitter() {
		return notifyFromByTwitter.isSelected();
	}

	public boolean isNotifyToByMail() {
		return notifyToByMail.isSelected();
	}

	public boolean isNotifyToBySMS() {
		return notifyToBySMS.isSelected();
	}

	public boolean isNotifyToByTwitter() {
		return notifyToByTwitter.isSelected();
	}

	public void setCode(final String code) {
		this.code.setText(code);
	}

	public void setDelivered(final boolean delivered) {
		this.delivered.setSelected(delivered);
	}

	public void setDescription(final String description) {
		this.description.setText(description);
	}

	public void setDetails(final String details) {
		this.details.setText(details);
	}

	public void setEstimative(final Double estimate) {
		if (estimate != null) {
			this.estimate.setValue(estimate);
		}
	}

	public void setFromList(final List<Person> people) {
		final ComboBoxModel oldModel = from.getModel();
		final Person selected = (Person) oldModel.getSelectedItem();
		final PersonComboBoxModel model = new PersonComboBoxModel();
		model.setPeople(people);
		from.setModel(model);
		if (selected != null) {
			from.getModel().setSelectedItem(selected);
		}
	}

	/**
	 * Definir quem é o remetente selecionado na lista.
	 * 
	 * @param person Pessoa a ser selecionada.
	 */
	public void setFrom(final Person person) {
		from.getModel().setSelectedItem(person);
		from.repaint();
	}

	public void setImportant(final boolean important) {
		this.important.setSelected(important);
	}

	public void setDeclaredValue(final Double declaredValue) {
		if (declaredValue != null) {
			this.declaredValue.setValue(declaredValue);
		}
	}

	public void setNotifyFromByMail(final boolean notify) {
		this.notifyFromByMail.setSelected(notify);
	}

	public void setNotifyFromBySMS(final boolean notify) {
		this.notifyFromBySMS.setSelected(notify);
	}

	public void setNotifyFromByTwitter(final boolean notify) {
		this.notifyFromByTwitter.setSelected(notify);
	}

	public void setNotifyToByMail(final boolean notify) {
		this.notifyToByMail.setSelected(notify);
	}

	public void setNotifyToBySMS(final boolean notify) {
		this.notifyToBySMS.setSelected(notify);
	}

	public void setNotifyToByTwitter(final boolean notify) {
		this.notifyToByTwitter.setSelected(notify);
	}

	public void setSent(final Date date) {
		sentDate.setDate(date);
	}

	public void setToList(final List<Person> people) {
		final ComboBoxModel oldModel = to.getModel();
		final Person selected = (Person) oldModel.getSelectedItem();
		final PersonComboBoxModel model = new PersonComboBoxModel();
		model.setPeople(people);
		to.setModel(model);
		if (selected != null) {
			to.getModel().setSelectedItem(selected);
		}
	}

	/**
	 * Definir quem é o destinatário selecionado na lista.
	 * 
	 * @param person Pessoa a ser selecionada.
	 */
	public void setTo(final Person person) {
		to.getModel().setSelectedItem(person);
		to.repaint();
	}

	public void setValue(final Double value) {
		if (value != null) {
			this.value.setValue(value);
		}
	}

	public void setWeight(final Double weight) {
		if (weight != null) {
			this.weight.setValue(weight);
		}
	}

	public void setPostalServicesList(List<PostalService> list) {
		final ComboBoxModel oldModel = postalServices.getModel();
		final Service selected = (Service) oldModel.getSelectedItem();
		final PostalServiceComboBoxModel model = new PostalServiceComboBoxModel();
		model.setPostalServices(list);
		postalServices.setModel(model);
		if (selected != null) {
			postalServices.getModel().setSelectedItem(selected);
		}
	}

	public void setPostalService(PostalService postalService) {
		this.postalServices.getModel().setSelectedItem(postalService);
		from.repaint();
	}

	public void setService(Service service) {
		this.services.getModel().setSelectedItem(service);
	}

	public PostalService getPostalService() {
		return (PostalService) postalServices.getSelectedItem();
	}

	public void setServicesList(List<Service> list) {
		final ServiceComboBoxModel model = new ServiceComboBoxModel();
		model.setServices(list);
		services.setModel(model);
		services.repaint();
	}

	public Service getService() {
		return (Service) services.getSelectedItem();
	}

	public Integer getGirth() {
		return girth.getValue().intValue();
	}

	public void setGirth(Integer value) {
		girth.setText(value.toString());
	}

	public Integer getAltitude() {
		return height.getValue().intValue();
	}

	public void setAltitude(Integer value) {
		height.setText(value.toString());
	}

	public Integer getBreadth() {
		return width.getValue().intValue();
	}

	public void setBreadth(Integer value) {
		width.setText(value.toString());
	}

	public Integer getLength() {
		return length.getValue().intValue();
	}

	public void setLength(Integer value) {
		length.setText(value.toString());
	}

	public boolean isArchived() {
		return archived.isSelected();
	}

	public void setArchived(boolean b) {
		archived.setSelected(b);
	}
}
