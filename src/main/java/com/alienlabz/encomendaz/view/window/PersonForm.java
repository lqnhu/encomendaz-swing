package com.alienlabz.encomendaz.view.window;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.enterprise.util.AnnotationLiteral;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.qualifiers.Deleted;
import com.alienlabz.encomendaz.bus.qualifiers.PostalCodeChanged;
import com.alienlabz.encomendaz.bus.qualifiers.Saved;
import com.alienlabz.encomendaz.configuration.InternalConfiguration;
import com.alienlabz.encomendaz.factory.FieldFactory;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;
import com.alienlabz.encomendaz.view.component.JRichTextField;

import net.miginfocom.swing.MigLayout;
import br.gov.frameworkdemoiselle.util.Strings;

/**
 * Formulário para o cadastro de pessoas.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@SuppressWarnings("serial")
public class PersonForm extends GenericCrudForm {
	private static final long serialVersionUID = 1L;
	private JButton buttonDelete;
	private JRichTextField name;
	private JRichTextField address1;
	private JRichTextField address2;
	private JRichTextField email;
	private JRichTextField twitter;
	private JRichTextField state;
	private JRichTextField district;
	private JRichTextField city;
	private JRichTextField country;
	private JRichTextField postalCode;
	private JCheckBox notifyByMail;
	private JCheckBox notifyBySMS;
	private JCheckBox notifyByTwitter;
	private JTabbedPane tabs;
	private JToolBar toolbar;
	private JPanel panelNotifications;
	private JPanel panelForm;

	public PersonForm() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		bundleImages = ResourceBundleFactory.getImagesBundle();
		initialize();
		initializeTabPane();
		initializeFields();
		initializePanelForm();
		initializePanelNotifications();
		initializeToolBar();
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(final WindowEvent e) {
				name.requestFocus();
			}
		});
	}

	@Override
	public void close() {
		setVisible(false);
	}

	public String getDistrict() {
		return district.getText();
	}
	
	public String getAddress1() {
		return address1.getText();
	}

	public String getAddress2() {
		return address2.getText();
	}

	public String getCity() {
		return city.getText();
	}

	public String getCountry() {
		return country.getText();
	}

	public String getEmail() {
		return email.getText();
	}

	public String getName() {
		return name.getText();
	}

	public String getPostalCode() {
		return postalCode.getText();
	}

	public String getState() {
		return state.getText();
	}

	public String getTwitter() {
		return twitter.getText();
	}

	private void initialize() {
		setLayout(new BorderLayout());
		setPreferredSize(InternalConfiguration.getPersonFormDimension());
		setResizable(true);
		setModal(true);
		setTitle(bundleMessages.getString("form.person.title"));
	}

	/**
	 * Inicializar todos os campos.
	 */
	private void initializeFields() {
		name = FieldFactory.richtext(bundleMessages.getString("dialog.person.form.name.hint"));
		address1 = FieldFactory.richtext(bundleMessages.getString("dialog.person.form.address1.hint"));
		address2 = FieldFactory.richtext(bundleMessages.getString("dialog.person.form.address2.hint"));
		email = FieldFactory.richtext(bundleMessages.getString("dialog.person.form.email.hint"));
		twitter = FieldFactory.richtext(bundleMessages.getString("dialog.person.form.twitter.hint"));
		state = FieldFactory.richtext(bundleMessages.getString("dialog.person.form.state.hint"));
		city = FieldFactory.richtext(bundleMessages.getString("dialog.person.form.city.hint"));
		district = FieldFactory.richtext(bundleMessages.getString("dialog.person.form.district.hint"));
		country = FieldFactory.richtext(bundleMessages.getString("dialog.person.form.country.hint"));
		postalCode = FieldFactory.richtext(bundleMessages.getString("dialog.person.form.postalCode.hint"));
		notifyByMail = FieldFactory.checkbox();
		notifyBySMS = FieldFactory.checkbox();
		notifyByTwitter = FieldFactory.checkbox();

		postalCode.addFocusListener(new FocusAdapter() {

			@Override
			public void focusLost(FocusEvent event) {
				if (!Strings.isEmpty(postalCode.getText())) {
					EventManager.fire(PersonForm.this, new AnnotationLiteral<PostalCodeChanged>() {
					});
				}
			}

		});
	}

	/**
	 * Inicializar o formulário, adicionando os campos e seus labels.
	 */
	private void initializePanelForm() {
		panelForm = new JPanel(new MigLayout("fillx", "[align right][grow]"));

		panelForm.add(new JLabel(bundleMessages.getString("dialog.person.form.name.label")), "gap 10");
		panelForm.add(name, "span, growx");

		panelForm.add(new JLabel(bundleMessages.getString("dialog.person.form.email.label")), "gap 10");
		panelForm.add(email, "span, growx");

//		panelForm.add(new JLabel(bundleMessages.getString("dialog.person.form.twitter.label")), "gap 10");
//		panelForm.add(twitter, "span, growx");

		panelForm.add(new JLabel(bundleMessages.getString("dialog.person.form.address1.label")), "gap 10");
		panelForm.add(address1, "span, growx");

		panelForm.add(new JLabel(bundleMessages.getString("dialog.person.form.address2.label")), "gap 10");
		panelForm.add(address2, "span, growx");

		panelForm.add(new JLabel(bundleMessages.getString("dialog.person.form.district.label")), "gap 10");
		panelForm.add(district, "span, growx");

		panelForm.add(new JLabel(bundleMessages.getString("dialog.person.form.postalCode.label")), "gap 10");
		panelForm.add(postalCode, "span, growx");

		panelForm.add(new JLabel(bundleMessages.getString("dialog.person.form.city.label")), "gap 10");
		panelForm.add(city, "span, growx");

		panelForm.add(new JLabel(bundleMessages.getString("dialog.person.form.state.label")), "gap 10");
		panelForm.add(state, "span, growx");

		panelForm.add(new JLabel(bundleMessages.getString("dialog.person.form.country.label")), "gap 10");
		panelForm.add(country, "span, growx");

		tabs.addTab("Pessoa", panelForm);
	}

	/**
	 * Inicializar o painel de notificações.
	 */
	private void initializePanelNotifications() {
		panelNotifications = new JPanel(new MigLayout("fillx", "[align right][grow]"));

		panelNotifications.add(new JLabel(bundleMessages.getString("dialog.person.form.notifyByMail.label")), "gap 10");
		panelNotifications.add(notifyByMail, "span, growx");

		panelNotifications.add(new JLabel(bundleMessages.getString("dialog.person.form.notifyBySMS.label")), "gap 10");
		panelNotifications.add(notifyBySMS, "span, growx");

		panelNotifications.add(new JLabel(bundleMessages.getString("dialog.person.form.notifyByTwitter.label")),
				"gap 10");
		panelNotifications.add(notifyByTwitter, "span, growx");

		tabs.addTab(bundleMessages.getString("form.person.title.notifications"), panelNotifications);
	}

	/**
	 * Inicializar o painel de abas.
	 */
	private void initializeTabPane() {
		tabs = new JTabbedPane();
		add(tabs, BorderLayout.CENTER);
	}

	/**
	 * Inicializar a toolbar contendo os botões de ações.
	 */
	private void initializeToolBar() {
		toolbar = new JToolBar();
		toolbar.setPreferredSize(new Dimension(0, 50));

		// Botão salvar.
		final JButton buttonSave = new JButton(bundleMessages.getString("button.text.save"));
		buttonSave.setForeground(Color.WHITE);
		buttonSave.setIcon(IconUtil.getImageIcon(bundleImages.getString("button.icon.save")));
		toolbar.add(buttonSave);
		buttonSave.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent event) {
				EventManager.fire(PersonForm.this, new AnnotationLiteral<Saved>() {
				});
			}

		});

		// Botão deletar.
		buttonDelete = new JButton(bundleMessages.getString("button.text.delete"));
		buttonDelete.setForeground(Color.WHITE);
		buttonDelete.setIcon(IconUtil.getImageIcon(bundleImages.getString("button.icon.delete")));
		toolbar.add(buttonDelete);
		buttonDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent event) {
				EventManager.fire(PersonForm.this, new AnnotationLiteral<Deleted>() {
				});
			}

		});

		// Botão Voltar.
		final JButton buttonBack = new JButton(bundleMessages.getString("button.text.back"));
		buttonBack.setForeground(Color.WHITE);
		buttonBack.setIcon(IconUtil.getImageIcon(bundleImages.getString("button.icon.back")));
		toolbar.add(buttonBack);
		buttonBack.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				PersonForm.this.setVisible(false);
			}

		});
		add(toolbar, BorderLayout.NORTH);
	}

	public boolean isNotifyByMail() {
		return notifyByMail.isSelected();
	}

	public boolean isNotifyBySMS() {
		return notifyBySMS.isSelected();
	}

	public boolean isNotifyByTwitter() {
		return notifyByTwitter.isSelected();
	}

	public void setAddress1(final String address1) {
		this.address1.setText(address1);
	}

	public void setAddress2(final String address2) {
		this.address2.setText(address2);
	}

	public void setCity(final String city) {
		this.city.setText(city);
	}

	public void setCountry(final String country) {
		this.country.setText(country);
	}

	public void setDeleteActionDisabled() {
		buttonDelete.setEnabled(false);
	}

	public void setDeleteActionEnabled() {
		buttonDelete.setEnabled(true);
	}

	public void setEmail(final String email) {
		this.email.setText(email);
	}

	public void setName(final String name) {
		this.name.setText(name);
	}

	public void setNotifyByMail(final boolean notify) {
		notifyByMail.setSelected(notify);
	}

	public void setNotifyBySMS(final boolean notify) {
		notifyBySMS.setSelected(notify);
	}

	public void setNotifyByTwitter(final boolean notify) {
		notifyByTwitter.setSelected(notify);
	}

	public void setPostalCode(final String postalCode) {
		this.postalCode.setText(postalCode);
	}

	public void setState(final String state) {
		this.state.setText(state);
	}

	public void setTwitter(final String twitter) {
		this.twitter.setText(twitter);
	}

	public void setDistrict(final String district) {
		this.district.setText(district);
	}
}
