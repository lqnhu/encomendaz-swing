package com.alienlabz.encomendaz.view.window;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.ResourceBundle;

import javax.enterprise.util.AnnotationLiteral;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import net.miginfocom.swing.MigLayout;

import org.divxdede.swing.busy.JBusyComponent;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.qualifiers.CalculatePrice;
import com.alienlabz.encomendaz.bus.qualifiers.PostalServiceChanged;
import com.alienlabz.encomendaz.configuration.InternalConfiguration;
import com.alienlabz.encomendaz.domain.PostalService;
import com.alienlabz.encomendaz.domain.Service;
import com.alienlabz.encomendaz.factory.FieldFactory;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;
import com.alienlabz.encomendaz.util.NumberUtil;
import com.alienlabz.encomendaz.view.component.JMoneyField;
import com.alienlabz.encomendaz.view.component.JNumberField;
import com.alienlabz.encomendaz.view.component.JRichTextField;
import com.alienlabz.encomendaz.view.component.JWeightField;
import com.alienlabz.encomendaz.view.model.PostalServiceComboBoxModel;
import com.alienlabz.encomendaz.view.model.ServiceComboBoxModel;

/**
 * Formulário para calcular o valor do frete.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class PriceForm extends JDialog {
	private ResourceBundle bundleImages;
	private ResourceBundle bundleMessages;
	private JPanel mainPanel;
	private JBusyComponent<JPanel> busy;
	private JComboBox postalServices;
	private JComboBox services;
	private JNumberField height;
	private JNumberField width;
	private JNumberField girth;
	private JNumberField length;
	private JMoneyField declaredValue;
	private JMoneyField value;
	private JWeightField weight;
	private JRichTextField from;
	private JRichTextField to;
	private JButton buttonCalcular;
	private JButton buttonBack;
	private JToolBar toolbar;

	public PriceForm() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		bundleImages = ResourceBundleFactory.getImagesBundle();
		initialize();
		initializeFields();
		initializeButtons();
	}

	/**
	 * Criar os botões.
	 */
	protected void initializeButtons() {
		toolbar = new JToolBar();
		toolbar.setPreferredSize(new Dimension(0, 50));

		// Botão salvar.
		buttonCalcular = new JButton(bundleMessages.getString("dialog.price.form.button.calculate.label"));
		buttonCalcular
				.setIcon(IconUtil.getImageIcon(bundleImages.getString("price.form.button.calculator.icon")));
		buttonCalcular.setForeground(InternalConfiguration.getDefaultButtonColor());
		toolbar.add(buttonCalcular);
		buttonCalcular.addActionListener(new ActionListener() {

			@Override
			@SuppressWarnings("serial")
			public void actionPerformed(final ActionEvent event) {
				EventManager.fire(PriceForm.this, new AnnotationLiteral<CalculatePrice>() {
				});
			}

		});

		// Botão Voltar.
		buttonBack = new JButton(bundleMessages.getString("button.text.back"));
		buttonBack.setForeground(InternalConfiguration.getDefaultButtonColor());
		buttonBack.setIcon(IconUtil.getImageIcon(bundleImages.getString("button.icon.back")));
		toolbar.add(buttonBack);
		buttonBack.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent arg0) {
				PriceForm.this.dispose();
			}

		});
		add(toolbar, BorderLayout.NORTH);
	}

	/**
	 * Criar os campos do formulário.
	 */
	private void initializeFields() {
		height = FieldFactory.number();
		width = FieldFactory.number();
		girth = FieldFactory.number();
		length = FieldFactory.number();
		value = FieldFactory.money();
		weight = FieldFactory.weight();
		from = FieldFactory.richtext("");
		to = FieldFactory.richtext("");
		postalServices = FieldFactory.combo();
		services = FieldFactory.combo();
		declaredValue = FieldFactory.money();

		postalServices.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent event) {
				EventManager.fire(PriceForm.this, new AnnotationLiteral<PostalServiceChanged>(){});
			}

		});

		value.setEnabled(false);
		value.setBackground(Color.green);

		mainPanel.add(new JLabel(bundleMessages.getString("dialog.price.form.postalservices.label")), "gap 10");
		mainPanel.add(postalServices, "span, growx");

		mainPanel.add(new JLabel(bundleMessages.getString("dialog.price.form.services.label")), "gap 10");
		mainPanel.add(services, "span, growx");

		mainPanel.add(new JLabel(bundleMessages.getString("dialog.price.form.from.label")), "gap 10");
		mainPanel.add(from, "span, growx");

		mainPanel.add(new JLabel(bundleMessages.getString("dialog.price.form.to.label")), "gap 10");
		mainPanel.add(to, "span, growx");

		mainPanel.add(new JLabel(bundleMessages.getString("dialog.price.form.width.label")), "gap 10");
		mainPanel.add(width, "span, growx");

		mainPanel.add(new JLabel(bundleMessages.getString("dialog.price.form.height.label")), "gap 10");
		mainPanel.add(height, "span, growx");

		mainPanel.add(new JLabel(bundleMessages.getString("dialog.price.form.length.label")), "gap 10");
		mainPanel.add(length, "span, growx");

		mainPanel.add(new JLabel(bundleMessages.getString("dialog.price.form.girth.label")), "gap 10");
		mainPanel.add(girth, "span, growx");

		mainPanel.add(new JLabel(bundleMessages.getString("dialog.price.form.weight.label")), "gap 10");
		mainPanel.add(weight, "span, growx");

		mainPanel.add(new JLabel(bundleMessages.getString("dialog.price.form.declaredvalue.label")), "gap 10");
		mainPanel.add(declaredValue, "span, growx");

		mainPanel.add(new JLabel(bundleMessages.getString("dialog.price.form.value.label")), "gap 10");
		mainPanel.add(value, "span, growx");
	}

	/**
	 * Inicializar propriedades da janela
	 */
	private void initialize() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setModal(true);
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(550, 400));
		setTitle(bundleMessages.getString("price.form.title"));
		mainPanel = new JPanel(new MigLayout("fillx", "[align left]"));
		busy = new JBusyComponent<JPanel>(mainPanel);
		add(busy, BorderLayout.CENTER);
	}

	public void setBusy(boolean pBusy) {
		busy.setBusy(pBusy);
	}

	public Integer getGirth() {
		return girth.getValue().intValue();
	}

	public void setGirth(Integer value) {
		girth.setText(value.toString());
	}

	public Integer getAltitude() {
		return height.getValue().intValue();
	}

	public void setAltitude(Integer value) {
		height.setText(value.toString());
	}

	public Integer getBreadth() {
		return width.getValue().intValue();
	}

	public void setBreadth(Integer value) {
		width.setText(value.toString());
	}

	public Integer getLength() {
		return length.getValue().intValue();
	}

	public void setLength(Integer value) {
		length.setText(value.toString());
	}

	public Double getValue() {
		return NumberUtil.convertCurrency(value.getText());
	}

	public Double getWeight() {
		return NumberUtil.toDouble(weight.getText());
	}

	public void setValue(final Double value) {
		if (value != null) {
			this.value.setValue(value);
		}
	}

	public void setWeight(final Double weight) {
		if (weight != null) {
			this.weight.setValue(weight);
		}
	}

	public void setPostalServicesList(List<PostalService> list) {
		final ComboBoxModel oldModel = postalServices.getModel();
		final Service selected = (Service) oldModel.getSelectedItem();
		final PostalServiceComboBoxModel model = new PostalServiceComboBoxModel();
		model.setPostalServices(list);
		postalServices.setModel(model);
		if (selected != null) {
			postalServices.getModel().setSelectedItem(selected);
		}
	}

	public void setPostalService(PostalService postalService) {
		this.postalServices.getModel().setSelectedItem(postalService);
		from.repaint();
	}

	public void setService(Service service) {
		this.services.getModel().setSelectedItem(service);
	}

	public PostalService getPostalService() {
		return (PostalService) postalServices.getSelectedItem();
	}

	public void setServicesList(List<Service> list) {
		final ServiceComboBoxModel model = new ServiceComboBoxModel();
		model.setServices(list);
		services.setModel(model);
		services.repaint();
	}

	public Service getService() {
		return (Service) services.getSelectedItem();
	}

	public String getFrom() {
		return from.getText();
	}

	public String getTo() {
		return to.getText();
	}

	public Double getDeclaredValue() {
		return declaredValue.getValue().doubleValue();
	}

	public void setDeclaredValue(Double declaredValue) {
		if (declaredValue != null) {
			this.declaredValue.setValue(declaredValue);
		}
	}

}
