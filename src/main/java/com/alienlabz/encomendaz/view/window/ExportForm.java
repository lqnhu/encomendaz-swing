package com.alienlabz.encomendaz.view.window;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.enterprise.util.AnnotationLiteral;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.border.TitledBorder;

import net.miginfocom.swing.MigLayout;

import org.divxdede.swing.busy.JBusyComponent;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.qualifiers.ExportCodes;
import com.alienlabz.encomendaz.bus.qualifiers.SaveToFile;
import com.alienlabz.encomendaz.configuration.InternalConfiguration;
import com.alienlabz.encomendaz.factory.FieldFactory;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;

/**
 * Janela de exportação de códigos.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.o
 */
public class ExportForm extends JDialog {
	private ResourceBundle bundleMessages;
	private ResourceBundle bundleImages;
	private JTextArea codes;
	private JComboBox separator;
	private JScrollPane scroll;
	private Map<String, String> separatorOptions;
	private JToolBar toolbar;
	private JPanel mainPanel;
	private JBusyComponent<JPanel> busy;
	private JButton buttonSave;

	public ExportForm() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		bundleImages = ResourceBundleFactory.getImagesBundle();
		initialize();
		initializeCodesPanel();
		initializeToolbar();
	}

	public void setBusy(boolean busy) {
		this.busy.setBusy(busy);
	}

	/**
	 * Criar os botões da toolbar.
	 */
	private void initializeToolbar() {
		toolbar = new JToolBar();
		toolbar.setPreferredSize(new Dimension(0, 50));
		JButton buttonImport = new JButton(bundleMessages.getString("export.generate.label"));
		buttonImport.setIcon(IconUtil.getImageIcon(bundleImages.getString("export.generate.icon")));
		buttonImport.setForeground(InternalConfiguration.getDefaultButtonColor());
		toolbar.add(buttonImport);
		buttonImport.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				EventManager.fire(ExportForm.this, new AnnotationLiteral<ExportCodes>() {
				});
			}

		});

		buttonSave = new JButton(bundleMessages.getString("export.save.label"));
		buttonSave.setEnabled(false);
		buttonSave.setIcon(IconUtil.getImageIcon(bundleImages.getString("export.save.icon")));
		buttonSave.setForeground(InternalConfiguration.getDefaultButtonColor());
		toolbar.add(buttonSave);
		buttonSave.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				EventManager.fire(ExportForm.this, new AnnotationLiteral<SaveToFile>() {
				});
			}

		});

		JButton buttonBack = new JButton(bundleMessages.getString("button.text.back"));
		buttonBack.setForeground(InternalConfiguration.getDefaultButtonColor());
		buttonBack.setIcon(IconUtil.getImageIcon(bundleImages.getString("button.icon.back")));
		toolbar.add(buttonBack);
		buttonBack.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent arg0) {
				ExportForm.this.setVisible(false);
			}

		});

		add(toolbar, BorderLayout.NORTH);
	}

	/**
	 * Inicializar o painel que vai exibir um textarea para o usuário colar os códigos.
	 */
	private void initializeCodesPanel() {
		separatorOptions = new HashMap<String, String>();
		separatorOptions.put(bundleMessages.getString("export.separator.option.comma"), ",");
		separatorOptions.put(bundleMessages.getString("export.separator.option.newline"), "\n");

		JPanel panel = new JPanel(new MigLayout("fillx", "[align right][grow]"));
		panel.setBorder(new TitledBorder(bundleMessages.getString("export.form.panel.codes.title")));

		panel.add(new JLabel(bundleMessages.getString("dialog.export.separator.label")), "gap 10");
		separator = FieldFactory.combo();
		separator.setModel(new DefaultComboBoxModel(separatorOptions.keySet().toArray()));
		panel.add(separator, "span, growx");

		codes = FieldFactory.textarea();
		codes.setRows(10);
		scroll = new JScrollPane(codes);
		scroll.setAutoscrolls(true);
		panel.add(scroll, "span, growx");
		mainPanel.add(panel, "span, growx");
	}

	/**
	 * Inicializar propriedades da janela
	 */
	private void initialize() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setModal(true);
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(550, 350));
		setTitle(bundleMessages.getString("export.form.title"));
		mainPanel = new JPanel(new MigLayout("fillx", "[align left]"));
		busy = new JBusyComponent<JPanel>(mainPanel);
		add(busy, BorderLayout.CENTER);
	}

	public String getSeparator() {
		return (String) separator.getSelectedItem();
	}

	public void setResult(final String result) {
		codes.setText(result);
	}

	public void enableButtonSave(final boolean enable) {
		buttonSave.setEnabled(enable);
	}

}
