package com.alienlabz.encomendaz.view.tray;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.enterprise.util.AnnotationLiteral;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.events.CloseApplication;
import com.alienlabz.encomendaz.bus.qualifiers.DisplayApplication;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;

/**
 * Construtor da opção de TrayIcon que será exibida na aplicação.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
final public class TrayIconManager {
	private static TrayIcon trayIcon;
	private static PopupMenu popup = new PopupMenu();
	private static SystemTray tray = SystemTray.getSystemTray();

	public static void start() {
		ResourceBundle bundleImages = ResourceBundleFactory.getImagesBundle();

		if (SystemTray.isSupported()) {
			Image defaultIcon = IconUtil.getImage(bundleImages.getString("default.tray.icon"));
			trayIcon = new TrayIcon(defaultIcon);
			createMenuItens();
			trayIcon.setPopupMenu(popup);
			try {
				tray.add(trayIcon);
			} catch (AWTException e) {
				e.printStackTrace();
			}
		}
	}

	public static void showMessage(final String message) {
		trayIcon.displayMessage("EncomendaZ", message, MessageType.INFO);
	}

	private static void createMenuItens() {
		final ResourceBundle bundleMessages = ResourceBundleFactory.getMessagesBundle();
		MenuItem openItem = new MenuItem(bundleMessages.getString("tray.open.title"));
		openItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				EventManager.fire(bundleMessages.getString("tray.open.title"),
						new AnnotationLiteral<DisplayApplication>() {
						});
			}

		});
		MenuItem exitItem = new MenuItem("Sair");
		exitItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				EventManager.fire(new CloseApplication());
			}

		});
		popup.add(openItem);
		popup.addSeparator();
		popup.add(exitItem);
	}

}
