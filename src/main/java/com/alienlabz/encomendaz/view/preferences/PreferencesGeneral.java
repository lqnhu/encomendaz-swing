package com.alienlabz.encomendaz.view.preferences;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import com.alienlabz.encomendaz.factory.FieldFactory;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.message.Messages;
import com.alienlabz.encomendaz.util.AsyncTask;
import com.alienlabz.encomendaz.util.AudioPlayer;
import com.alienlabz.encomendaz.util.IconUtil;
import com.alienlabz.encomendaz.util.NumberUtil;

import net.miginfocom.swing.MigLayout;

/**
 * Trata das preferências gerais da aplicação.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class PreferencesGeneral extends JPanel {
	private static final long serialVersionUID = 1L;
	private ResourceBundle bundleMessages;
	private ResourceBundle bundleImages;
	private JSpinner refreshRate;
	private JFileChooser soundFileChooser;
	private JTextField soundFile;
	private JCheckBox sendEmails;

	public PreferencesGeneral() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		bundleImages = ResourceBundleFactory.getImagesBundle();
		setLayout(new MigLayout("fillx", "[align left][align center, grow][align center][align center]"));

		initializeSoundFileChooser();
		initializeRefreshDelay();
	}

	/**
	 * Inicializar o campo de escolha do arquivo de som.
	 */
	private void initializeSoundFileChooser() {
		JPanel panel = new JPanel(new MigLayout("fillx", "[grow][align center][align center]"));
		panel.setBorder(new TitledBorder(bundleMessages.getString("preferences.general.soundfile.label")));

		soundFile = FieldFactory.text();
		JButton buttonSelectFile = FieldFactory.button(bundleMessages.getString("generic.button.select"));
		JButton buttonPlay = FieldFactory.button("");
		buttonPlay.setIcon(IconUtil.getImageIcon(bundleImages.getString("generic.button.play.icon")));
		soundFileChooser = FieldFactory.fileChooser();

		panel.add(soundFile, "growx");
		panel.add(buttonPlay, "growx");
		panel.add(buttonSelectFile, "span, growx");

		JLabel label = new JLabel(bundleMessages.getString("preferences.general.soundfile.description"));
		label.setFont(Font.decode("Verdana-ITALIC-10"));
		label.setHorizontalTextPosition(SwingConstants.CENTER);
		panel.add(label, "span, growx");

		add(panel, "span, growx");

		buttonPlay.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				if (soundFile.getText() != null && !soundFile.getText().equals("")) {
					new AsyncTask<Boolean, Void>() {

						@Override
						protected Boolean doTask() {
							try {
								AudioPlayer.play(soundFile.getText());
								return true;
							} catch (Exception e) {
								return false;
							}
						}

						@Override
						protected void onPostExecute(Boolean result) {
							if (!result) {
								Messages.info(bundleMessages.getString("exception.cant.play.sound"));
							}
						}
					}.execute();
				}
			}

		});
		buttonSelectFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				int returnVal = soundFileChooser.showOpenDialog(PreferencesGeneral.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					soundFile.setText(soundFileChooser.getSelectedFile().getAbsolutePath());
				}
			}

		});
	}

	/**
	 * Inicializar o campo Refresh Delay.
	 */
	private void initializeRefreshDelay() {
		JPanel panel = new JPanel(new MigLayout("fillx", "[grow][align center][align center]"));
		panel.setBorder(new TitledBorder(bundleMessages.getString("preferences.general.refreshdelay.label")));

		refreshRate = FieldFactory.spinner(new SpinnerNumberModel());

		panel.add(refreshRate, "span, growx");

		JLabel label = new JLabel(bundleMessages.getString("preferences.general.refreshdelay.description"));
		label.setFont(Font.decode("Verdana-ITALIC-10"));
		label.setHorizontalTextPosition(SwingConstants.CENTER);
		panel.add(label, "span, growx");

		sendEmails = FieldFactory.checkbox();
		sendEmails.setText("Enviar e-mails automaticamente quando houver mudanças?");

		panel.add(sendEmails);

		add(panel, "span, growx");
	}

	public int getRefreshRate() {
		return NumberUtil.toInteger(refreshRate.getValue().toString());
	}

	public String getNotificationSound() {
		return soundFile.getText();
	}

	public boolean isSendAutomaticEmails() {
		return sendEmails.isSelected();
	}

	public void setSendAutomaticEmails(boolean send) {
		sendEmails.setSelected(send);
	}

	public void setRefreshRate(int refreshRate2) {
		refreshRate.setValue(refreshRate2);
	}

	public void setNotificationSound(String notificationSound) {
		soundFile.setText(notificationSound);
	}

}
