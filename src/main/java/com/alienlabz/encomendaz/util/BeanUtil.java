package com.alienlabz.encomendaz.util;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;

/**
 * Utilitário para manipular beans sem precisar lançar exceções checadas.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
final public class BeanUtil {

	/**
	 * Copiar as propiedades de um bean para outro.
	 * 
	 * @param dest
	 * @param origin
	 */
	public static void copyProperties(Object dest, Object origin) {
		try {
			BeanUtils.copyProperties(dest, origin);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

}
