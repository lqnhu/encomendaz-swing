package com.alienlabz.encomendaz.util;

import com.alienlabz.encomendaz.domain.State;
import com.alienlabz.encomendaz.domain.Tracking;

/**
 * Utilitário para encontrar qual a imagem respectiva para o estado da encomenda.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
final public class StateUtil {

	/**
	 * Encontra qual a imagem correspondente para o estado atual da encomenda.
	 * 
	 * @param tracking Encomenda.
	 * @return Imagem.
	 */
	public static String getStateIcon(Tracking tracking) {
		String icon = null;
		State state = tracking.getState();
		if (state == State.BadRouted) {
			icon = "bad_routed.png";
		} else if (state == State.Checked) {
			icon = "checked.png";
		} else if (state == State.Delivered) {
			icon = "delivered.png";
		} else if (state == State.InSupervision) {
			icon = "insupervision.png";
		} else if (state == State.InSupervisionArmy) {
			icon = "insupervisionarmy.png";
		} else if (state == State.InternationalReturn) {
			icon = "return.png";
		} else if (state == State.OutForDelivery) {
			icon = "outfordelivery.png";
		} else if (state == State.Posted) {
			icon = "posted.png";
		} else if (state == State.Returned) {
			icon = "return.png";
		} else if (state == State.Routed) {
			icon = "routed.png";
		} else if (state == State.Unregistered) {
			icon = "unregistered.png";
		} else if (state == State.Unsought) {
			icon = "unsought.png";
		} else if (state == State.WaitingReceiver) {
			icon = "waitingreceiver.png";
		}
		return icon;
	}

}
