package com.alienlabz.encomendaz.util;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.JComponent;
import javax.swing.ToolTipManager;

/**
 * Utilitário para funções comuns em Swing.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class SwingUtil {

	/**
	 * Centralizar uma janela.
	 * 
	 * @param frame Janela a ser centralizada.
	 */
	public static void center(final Window frame) {
		final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		final Dimension window = frame.getSize();
		if (window.height > screen.height) {
			window.height = screen.height;
		}
		if (window.width > screen.width) {
			window.width = screen.width;
		}
		final int xCoord = (screen.width / 2 - window.width / 2);
		final int yCoord = (screen.height / 2 - window.height / 2);
		frame.setLocation(xCoord, yCoord);
	}

	/**
	 * Centralizar uma janela considerando a dimensão total da tela passada como parâmetro.
	 * 
	 * @param frame Janela a ser centralizada.
	 * @param window Dimensão total da tela.
	 */
	public static void center(final Window frame, final Dimension window) {
		final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		if (window.height > screen.height) {
			window.height = screen.height;
		}
		if (window.width > screen.width) {
			window.width = screen.width;
		}
		final int xCoord = (screen.width / 2 - window.width / 2);
		final int yCoord = (screen.height / 2 - window.height / 2);
		frame.setLocation(xCoord, yCoord);
	}

	public static void setToolTip(String text, JComponent component) {
		ToolTipManager.sharedInstance().setInitialDelay(0);
		ToolTipManager.sharedInstance().setDismissDelay(10000);
		component.setToolTipText("<html><table><tr><td><img src='" + SwingUtil.class.getResource("/icons/tip.png") + "'/></td><td>" + text + "</td></tr></table></html>");
	}

}
