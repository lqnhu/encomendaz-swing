package com.alienlabz.encomendaz.presenter;

import java.util.List;

import javax.enterprise.event.Observes;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.JOptionPane;
import javax.validation.ConstraintViolationException;

import org.alfredlibrary.AlfredException;
import org.alfredlibrary.utilitarios.correios.CEP;
import org.slf4j.Logger;

import br.gov.frameworkdemoiselle.annotation.Name;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import br.gov.frameworkdemoiselle.util.ResourceBundle;
import br.gov.frameworkdemoiselle.util.Strings;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.qualifiers.AfterDeleted;
import com.alienlabz.encomendaz.bus.qualifiers.AfterSaved;
import com.alienlabz.encomendaz.bus.qualifiers.Deleted;
import com.alienlabz.encomendaz.bus.qualifiers.ListingRequired;
import com.alienlabz.encomendaz.bus.qualifiers.Modify;
import com.alienlabz.encomendaz.bus.qualifiers.NewRequired;
import com.alienlabz.encomendaz.bus.qualifiers.PostalCodeChanged;
import com.alienlabz.encomendaz.bus.qualifiers.Saved;
import com.alienlabz.encomendaz.business.PersonBC;
import com.alienlabz.encomendaz.configuration.InternalConfiguration;
import com.alienlabz.encomendaz.domain.Person;
import com.alienlabz.encomendaz.domain.PersonPreferences;
import com.alienlabz.encomendaz.exceptions.ExceptionHandler;
import com.alienlabz.encomendaz.util.AsyncTask;
import com.alienlabz.encomendaz.util.BeanUtil;
import com.alienlabz.encomendaz.util.SwingUtil;
import com.alienlabz.encomendaz.view.window.PeopleListing;
import com.alienlabz.encomendaz.view.window.PersonForm;

/**
 * Responsável em tratar os eventos e a inicialização da tela de cadastro de Pessoas.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@ViewController
@Singleton
public class PersonPresenter {
	private PersonForm personForm;
	private PeopleListing listing;

	@Inject
	@Name("messages")
	private ResourceBundle bundleMessages;

	@Inject
	private PersonBC personBC;

	@Inject
	private Logger logger;

	private Person person;

	/**
	 * Capturar o evento que solicita a edição de uma pessoa.
	 * 
	 * @param person Pessoa a ser editada.
	 */
	public void modify(@Observes @Modify Person person) {
		logger.debug("tratando o evento Modify.");
		PersonForm form = new PersonForm();
		BeanUtil.copyProperties(form, person);
		if (person.getPreferences() != null) {
			BeanUtil.copyProperties(form, person.getPreferences());
		}
		this.person = person;
		form.pack();
		SwingUtil.center(form);
		form.setVisible(true);
	}

	/**
	 * Tratar o evento que solicita a tela de listagem de pessoas.
	 * 
	 * @param person Pessoa.
	 */
	public void listingRequired(@Observes @ListingRequired final Person person) {
		listing = new PeopleListing();
		listing.pack();
		SwingUtil.center(listing);
		listing.setPeople(personBC.findAll());
		listing.setVisible(true);
	}

	/**
	 * Tratar o evento que solicita a criação e exibição de uma janela de cadastro para pessoas.
	 * 
	 * @param event Evento.
	 */
	public void personFormRequired(@Observes @NewRequired final Person person) {
		this.person = new Person();
		personForm = new PersonForm();
		personForm.pack();
		SwingUtil.center(personForm, InternalConfiguration.getPersonFormDimension());
		personForm.setDeleteActionDisabled();
		personForm.setVisible(true);
	}

	/**
	 * Capturar o evento que solicita o salvamento de uma entidade Person.
	 * 
	 * @param event Evento.
	 */
	public void savePerson(@Observes @Saved final PersonForm personForm) {
		logger.debug("tratando o evento de salvamento de pessoas (!). esta porra é sistema pra bombeiros, é?");

		final Person person = this.person == null ? new Person() : this.person;
		BeanUtil.copyProperties(person, personForm);

		PersonPreferences prefs;
		if (person.getPreferences() == null) {
			prefs = new PersonPreferences();
		} else {
			prefs = person.getPreferences();
		}
		BeanUtil.copyProperties(prefs, personForm);
		person.setPreferences(prefs);

		new AsyncTask<List<Person>, Void>() {

			@Override
			@SuppressWarnings("serial")
			protected List<Person> doTask() {
				logger.debug("solicitando ao personbc a inserção da pessoa.");
				try {
					if (person.getId() == null) {
						personBC.insert(person);
					} else {
						personBC.update(person);
					}
				} catch (final ConstraintViolationException exception) {
					ExceptionHandler.handle(exception);
					logger.info("não foi possível inserir a pessoa devido a erros de validação.");
					return null;
				}
				logger.debug("pessoa inserida com sucesso. fechando a janela e lançando evento aftersaveperson.");
				personForm.close();
				EventManager.fire(person, new AnnotationLiteral<AfterSaved>() {
				});
				return personBC.findAll();
			}

			protected void onPostExecute(java.util.List<Person> result) {
				PersonPresenter.this.person = null;
				if (listing != null) {
					listing.setPeople(result);
				}
			}

		}.execute();
	}

	public void deleted(@Observes @Deleted final PersonForm personForm) {
		final Person person = new Person();
		person.setId(this.person.getId());
		BeanUtil.copyProperties(person, personForm);
		if (deleted(person)) {
			personForm.close();
		}
	}

	/**
	 * Tratar o evento que solicita a exclusão de uma pessoa.
	 * 
	 * @param person Pessoa a ser excluída.
	 */
	public boolean deleted(@Observes @Deleted final Person person) {
		boolean result = false;
		boolean existsPersonByTracking = personBC.existsPersonByTracking(person);
		final boolean forceDelete;
		final int resp;
		if (!existsPersonByTracking) {
			resp = JOptionPane.showConfirmDialog(null, bundleMessages.getString("generic.confirm.exclusion"),
					bundleMessages.getString("generic.confirm.title"), JOptionPane.YES_NO_OPTION);
			forceDelete = false;
		} else {
			resp = JOptionPane.showConfirmDialog(null, bundleMessages.getString("confirm.person.associated.exclusion"),
					bundleMessages.getString("generic.confirm.title"), JOptionPane.YES_NO_OPTION);
			if (resp == JOptionPane.YES_OPTION) {
				forceDelete = true;
			} else {
				forceDelete = false;
			}
		}
		if (resp == JOptionPane.YES_OPTION) {
			new AsyncTask<Void, Void>() {

				@Override
				protected Void doTask() {
					if (!forceDelete) {
						personBC.delete(person.getId());
					} else {
						personBC.forceDelete(person.getId());
					}
					return null;
				}

				@Override
				protected void onPostExecute(Void result) {
					if (listing != null) {
						listing.setPeople(personBC.findAll());
					}
					PersonPresenter.this.person = null;
					EventManager.fire(person, new AnnotationLiteral<AfterDeleted>() {
					});
				}

			}.execute();
			result = true;
		}
		return result;
	}

	/**
	 * Capturar o evento que informa que o código postal foi alterado.
	 * 
	 * @param postlaCode Código postal informado.
	 */
	public void postalCodeChanged(@Observes @PostalCodeChanged final PersonForm personForm) {
		if (!Strings.isEmpty(personForm.getPostalCode())) {
			new AsyncTask<String[], Void>() {

				@Override
				protected String[] doTask() {
					return CEP.consultarEnderecoCorreios(personForm.getPostalCode());
				}

				@Override
				protected void onPostExecute(String[] data) {
					try {
						if (personForm != null) {
							personForm.setAddress1(data[1]);
							personForm.setDistrict(data[2]);
							personForm.setCity(data[3] != null ? data[3].split("/")[0].trim() : "");
							personForm.setState(data[3] != null ? data[3].split("/")[1].trim() : "");
							personForm.setCountry("Brasil");
						}
					} catch (AlfredException exception) {
					}
				}
			}.execute();
		}
	}

}
