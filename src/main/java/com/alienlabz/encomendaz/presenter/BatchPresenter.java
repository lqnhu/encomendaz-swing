package com.alienlabz.encomendaz.presenter;

import javax.enterprise.event.Observes;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;
import javax.inject.Singleton;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.events.BatchCodesInsertion;
import com.alienlabz.encomendaz.bus.events.ShowBatchWindow;
import com.alienlabz.encomendaz.bus.qualifiers.AfterSaved;
import com.alienlabz.encomendaz.business.TrackingBC;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.util.AsyncTask;
import com.alienlabz.encomendaz.util.SwingUtil;
import com.alienlabz.encomendaz.view.window.BatchForm;

import br.gov.frameworkdemoiselle.stereotype.ViewController;

/**
 * Responsável em tratar os eventos oriundos do formulário de cadastro de códigos em lote.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@ViewController
@Singleton
public class BatchPresenter {

	private BatchForm batchForm;

	@Inject
	private TrackingBC trackingBC;

	/**
	 * Capturar o evento que solicita a abertura da janela de cadastro em lote.
	 * 
	 * @param event Evento.
	 */
	public void showBatchWindow(@Observes ShowBatchWindow event) {
		batchForm = new BatchForm();
		batchForm.pack();
		SwingUtil.center(batchForm);
		batchForm.setVisible(true);
	}

	/**
	 * Tratar o evento que solicita a inclusão dos códigos em lote.
	 * 
	 * @param event Evento.
	 */
	public void batchCodeInsertion(@Observes final BatchCodesInsertion event) {
		new AsyncTask<Void, Void>() {

			protected void onPreExecute() {
				BatchPresenter.this.batchForm.setBusy(true);
			}

			@Override
			protected Void doTask() {
				trackingBC.batchImport(event.getCodes(), event.getSeparator());
				return null;
			}

			protected void onPostExecute(Void result) {
				EventManager.fire(new Tracking(), new AnnotationLiteral<AfterSaved>() {
				});
				BatchPresenter.this.batchForm.setBusy(false);
				BatchPresenter.this.batchForm.setVisible(false);
			}

		}.execute();
	}

}
