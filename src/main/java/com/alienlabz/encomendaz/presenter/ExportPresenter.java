package com.alienlabz.encomendaz.presenter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.JFileChooser;

import br.gov.frameworkdemoiselle.annotation.Name;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import br.gov.frameworkdemoiselle.util.ResourceBundle;

import com.alienlabz.encomendaz.bus.events.ShowExport;
import com.alienlabz.encomendaz.bus.qualifiers.ExportCodes;
import com.alienlabz.encomendaz.bus.qualifiers.SaveToFile;
import com.alienlabz.encomendaz.business.TrackingBC;
import com.alienlabz.encomendaz.message.Messages;
import com.alienlabz.encomendaz.util.SwingUtil;
import com.alienlabz.encomendaz.view.window.ExportForm;

/**
 * Controlador responsável pelas ações da tela de exportação de códigos.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@Singleton
@ViewController
public class ExportPresenter {
	private ExportForm exportForm;

	@Inject
	private TrackingBC trackingBC;

	@Inject
	@Name("messages")
	private ResourceBundle bundleMessages;

	private Map<String, String> separatorOptions;

	/**
	 * Tratar o evento para exibir a janela de exportação.
	 * 
	 * @param event Evento.
	 */
	public void showExportForm(@Observes final ShowExport event) {
		separatorOptions = new HashMap<String, String>();
		separatorOptions.put(bundleMessages.getString("batch.separator.option.comma"), ",");
		separatorOptions.put(bundleMessages.getString("batch.separator.option.dot"), ".");
		separatorOptions.put(bundleMessages.getString("batch.separator.option.colon"), ":");
		separatorOptions.put(bundleMessages.getString("batch.separator.option.semicolon"), ";");
		separatorOptions.put(bundleMessages.getString("batch.separator.option.newline"), "\n");
		exportForm = new ExportForm();
		exportForm.pack();
		SwingUtil.center(exportForm);
		exportForm.setVisible(true);
	}

	/**
	 * Realizar a exportação dos códigos.
	 * 
	 * @param exportForm Formulário solicitante da exportação.
	 */
	public void exportCodes(@Observes @ExportCodes final ExportForm exportForm) {
		exportForm.setResult(trackingBC.getCodesToExport(separatorOptions.get(exportForm.getSeparator())));
		exportForm.enableButtonSave(true);
	}

	/**
	 * Salvar os códigos em arquivo.
	 * 
	 * @param exportForm
	 */
	public void saveCodes(@Observes @SaveToFile final ExportForm exportForm) {
		final JFileChooser fileChooser = new JFileChooser();
		final int returnVal = fileChooser.showOpenDialog(exportForm);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			final File path = fileChooser.getSelectedFile();
			try {
				FileWriter fstream = new FileWriter(path);
				BufferedWriter out = new BufferedWriter(fstream);
				out.write(trackingBC.getCodesToExport(separatorOptions.get(exportForm.getSeparator())));
				out.close();
				fstream.close();
				Messages.info(bundleMessages.getString("success.save.export.file"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
