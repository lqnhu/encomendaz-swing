package com.alienlabz.encomendaz.presenter;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.JFileChooser;

import br.gov.frameworkdemoiselle.stereotype.ViewController;

import com.alienlabz.encomendaz.bus.events.ShowReportsWindow;
import com.alienlabz.encomendaz.bus.qualifiers.Clear;
import com.alienlabz.encomendaz.bus.qualifiers.GenerateReport;
import com.alienlabz.encomendaz.bus.qualifiers.PostalServiceChanged;
import com.alienlabz.encomendaz.bus.qualifiers.PrintLabel;
import com.alienlabz.encomendaz.bus.qualifiers.ReportTemplatesChanged;
import com.alienlabz.encomendaz.bus.qualifiers.Searched;
import com.alienlabz.encomendaz.business.PersonBC;
import com.alienlabz.encomendaz.business.PostalServiceBC;
import com.alienlabz.encomendaz.business.ServiceBC;
import com.alienlabz.encomendaz.business.TrackingBC;
import com.alienlabz.encomendaz.domain.Service;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.message.Messages;
import com.alienlabz.encomendaz.report.ReportsGenerator;
import com.alienlabz.encomendaz.util.AsyncTask;
import com.alienlabz.encomendaz.util.SearchBundle;
import com.alienlabz.encomendaz.util.SwingUtil;
import com.alienlabz.encomendaz.view.window.ReportsForm;

/**
 * Responsável em tratar os eventos e a inicialização da tela de relatórios.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@ViewController
@Singleton
public class ReportsPresenter {
	public static final String ENTREGUES = "ENTREGUES";
	public static final String NAO_ENTREGUES = "NAOENTREGUES";
	public static final String ATRASADAS = "ATRASADAS";

	@Inject
	private PostalServiceBC postalServiceBC;

	@Inject
	private ReportsGenerator reports;

	@Inject
	private ServiceBC serviceBC;

	@Inject
	private PersonBC personBC;

	@Inject
	private TrackingBC trackingBC;

	private ResourceBundle bundleMessages;

	private ReportsForm reportsForm;

	private HashMap<String, String> templatesOptions;

	public ReportsPresenter() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();

		// FIXME Ooops! A pressa fez isso, não foi eu! Mas vou corrigir depois! Juro! :)
		templatesOptions = new HashMap<String, String>();
		templatesOptions.put("Encomendas Atrasadas", "ATRASADAS");
		templatesOptions.put("Encomendas Entregues", "ENTREGUES");
		templatesOptions.put("Encomendas Não Entregues", "NAOENTREGUES");
	}

	/**
	 * Observar o evento que solicita a pesquisa de rastreamentos.
	 * 
	 * @param reportsForm Formulário de pesquisa.
	 */
	public void searched(@Observes @Searched ReportsForm reportsForm) {
		Tracking tracking = new Tracking();
		tracking.setCode(reportsForm.getCode());
		tracking.setDescription(reportsForm.getDescription());
		tracking.setImportant(reportsForm.isImportant());
		tracking.setTaxed(reportsForm.isTaxed());
		tracking.setFrom(reportsForm.getFrom());
		tracking.setTo(reportsForm.getTo());
		tracking.setState(reportsForm.getState());
		tracking.setSent(reportsForm.getSent());
		tracking.setArchived(reportsForm.isArchived());
		Service service = reportsForm.getService();
		if (service == null) {
			service = new Service();
		}
		service.setPostalService(reportsForm.getPostalService());
		tracking.setService(service);

		reportsForm.setResult(trackingBC.search(tracking));
		reportsForm.setGeneratePDFEnabled(true);
	}

	/**
	 * Aguardar o evento para geração do relatório.
	 * 
	 * @param reportsForm Formulário de Relatório.
	 */
	public void generateReport(@Observes @GenerateReport final ReportsForm reportsForm) {
		final JFileChooser fileChooser = new JFileChooser();
		fileChooser.setSelectedFile(new File("relatorio.pdf"));
		fileChooser.setApproveButtonText(bundleMessages.getString("filechooser.approve.button.text"));
		final int returnVal = fileChooser.showOpenDialog(reportsForm);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			final String path = fileChooser.getSelectedFile().getAbsolutePath();

			//FIXME Tenho uma sensação que esse código pode ficar mais bonito... hummm...
			final String template = reportsForm.getTemplate();
			if (template != null) {
				if (ATRASADAS.equals(templatesOptions.get(template))) {
					reports.createOverdue(path);
				} else if (ENTREGUES.equals(templatesOptions.get(template))) {
					reports.createDelivered(path);
				} else if (NAO_ENTREGUES.equals(templatesOptions.get(template))) {
					reports.createUndelivered(path);
				}
			} else {
				final Tracking tracking = new Tracking();
				tracking.setCode(reportsForm.getCode());
				tracking.setDescription(reportsForm.getDescription());
				tracking.setImportant(reportsForm.isImportant());
				tracking.setTaxed(reportsForm.isTaxed());
				tracking.setFrom(reportsForm.getFrom());
				tracking.setTo(reportsForm.getTo());
				tracking.setState(reportsForm.getState());
				tracking.setSent(reportsForm.getSent());
				tracking.setArchived(reportsForm.isArchived());
				Service service = reportsForm.getService();
				if (service == null) {
					service = new Service();
				}
				service.setPostalService(reportsForm.getPostalService());
				tracking.setService(service);
				reports.createCustom(path, tracking);
			}
			Messages.info(bundleMessages.getString("message.report.success"));
		}
	}

	/**
	 * Limpar o formulário.
	 * 
	 * @param reportsForm Formulário de Relatórios.
	 */
	public void clearForm(@Observes @Clear final ReportsForm reportsForm) {
		reportsForm.clearForm();
		reportsForm.clearTemplates();
		reportsForm.setGeneratePDFEnabled(false);
		reportsForm.setSearchButtonEnabled(true);
	}

	/**
	 * Observar o evento que solicita a janela de pesquisa de rastreamentos.
	 * 
	 * @param event Evento.
	 */
	public void showReportsForm(@Observes ShowReportsWindow event) {
		reportsForm = new ReportsForm();
		reportsForm.pack();
		reportsForm.setPostalServicesList(postalServiceBC.findAll());
		reportsForm.setFromList(personBC.findAll());
		reportsForm.setToList(personBC.findAll());
		reportsForm.setStatesList(trackingBC.getAllStates());
		reportsForm.setTemplatesList(templatesOptions.keySet().toArray());
		SwingUtil.center(reportsForm);
		reportsForm.setGeneratePDFEnabled(false);
		reportsForm.setSearchButtonEnabled(true);
		reportsForm.setVisible(true);
	}

	/**
	 * Capturar o evento lançado quando o usuário muda o serviço postal.
	 * 
	 * @param event Evento.
	 */
	public void postalServiceChanged(@Observes @PostalServiceChanged final ReportsForm reportsForm) {
		if (reportsForm.getPostalService() != null) {
			new AsyncTask<List<Service>, Void>() {

				@Override
				protected List<Service> doTask() {
					return serviceBC.findByPostalService(reportsForm.getPostalService().getId());
				}

				protected void onPostExecute(java.util.List<Service> result) {
					reportsForm.setServicesList(result);
				}

			}.execute();
		}
	}

	/**
	 * Tratar quando o combo de templates muda.
	 * 
	 * @param reportsForm Formulário que contém o combo.
	 */
	public void reportTemplatesChanged(@Observes @ReportTemplatesChanged final ReportsForm reportsForm) {
		reportsForm.setGeneratePDFEnabled(true);
		reportsForm.setSearchButtonEnabled(false);
		reportsForm.clearForm();
		final String template = reportsForm.getTemplate();
		if (template != null) {
			if (ATRASADAS.equals(templatesOptions.get(template))) {
				reportsForm.setResult(trackingBC.findOverdueTrackings(new SearchBundle()));
			} else if (ENTREGUES.equals(templatesOptions.get(template))) {
				reportsForm.setResult(trackingBC.findDeliveredTrackings(new SearchBundle()));
			} else if (NAO_ENTREGUES.equals(templatesOptions.get(template))) {
				reportsForm.setResult(trackingBC.findNotDeliveredTrackings(new SearchBundle()));
			}
		}
	}

	/**
	 * Imprimir a etiqueta de uma determinada encomenda.
	 * 
	 * @param tracking Encomenda.
	 */
	public void printLabel(@Observes @PrintLabel final Tracking tracking) {
		final JFileChooser fileChooser = new JFileChooser();
		fileChooser.setSelectedFile(new File("etiqueta-" + tracking.getCode() + ".pdf"));
		fileChooser.setApproveButtonText(bundleMessages.getString("filechooser.approve.button.text"));
		final int returnVal = fileChooser.showOpenDialog(reportsForm);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			final String path = fileChooser.getSelectedFile().getAbsolutePath();

			new AsyncTask<Void, Void>() {

				@Override
				protected Void doTask() {
					reports.createLabel(path, tracking);
					return null;
				}

				protected void onPostExecute(Void result) {
					Messages.info(bundleMessages.getString("message.report.label.success"));
				}
			}.execute();

		}
	}
}
