package com.alienlabz.encomendaz.factory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;


import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.events.ListDelivered;
import com.alienlabz.encomendaz.bus.events.ListImportants;
import com.alienlabz.encomendaz.bus.events.ListInTransit;
import com.alienlabz.encomendaz.bus.events.ListNotArchived;
import com.alienlabz.encomendaz.bus.events.ListNotDelivered;
import com.alienlabz.encomendaz.bus.events.ListRecent;
import com.alienlabz.encomendaz.bus.events.ListUnregistered;
import com.alienlabz.encomendaz.util.IconUtil;
import com.l2fprod.common.swing.JButtonBar;
import com.l2fprod.common.swing.plaf.misc.IconPackagerButtonBarUI;

/**
 * Responsável em criar o barra de botões da lateral esquerda da aplicação.
 * 
 * Existe uma classe específica para isto por motivos de clareza e limpeza do código,
 * uma vez que é necessário muito código apenas para criar as opções.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class ButtonBarFactory {
	private final ResourceBundle messagesBundle;
	private final ResourceBundle imagesBundle;

	public ButtonBarFactory() {
		messagesBundle = ResourceBundleFactory.getMessagesBundle();
		imagesBundle = ResourceBundleFactory.getImagesBundle();
	}

	public JComponent build() {
		final JButtonBar buttonBar = new JButtonBar(SwingConstants.VERTICAL);
		JScrollPane scrollPane = new JScrollPane(buttonBar);
		scrollPane.setAutoscrolls(true);

		final ButtonGroup group = new ButtonGroup();

		buttonBar.setUI(new IconPackagerButtonBarUI());

		final JToggleButton buttonAllTrackings = new JToggleButton(
				messagesBundle.getString("buttonbar.button.alltrackings"), IconUtil.getImageIcon(imagesBundle
						.getString("buttonbar.button.alltrackings.icon")));
		buttonBar.add(buttonAllTrackings);
		group.add(buttonAllTrackings);
		buttonAllTrackings.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent event) {
				EventManager.fire(new ListNotArchived());
			}

		});
		buttonAllTrackings.setSelected(true);

		final JToggleButton buttonDelivered = new JToggleButton(
				messagesBundle.getString("buttonbar.button.deliveredtrackings"), IconUtil.getImageIcon(imagesBundle
						.getString("buttonbar.button.deliveredtrackings.icon")));
		buttonBar.add(buttonDelivered);
		group.add(buttonDelivered);
		buttonDelivered.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent event) {
				EventManager.fire(new ListDelivered());
			}

		});

		final JToggleButton buttonDelivering = new JToggleButton(
				messagesBundle.getString("buttonbar.button.deliveringtrackings"), IconUtil.getImageIcon(imagesBundle
						.getString("buttonbar.button.deliveringtrackings.icon")));
		buttonBar.add(buttonDelivering);
		group.add(buttonDelivering);
		buttonDelivering.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent event) {
				EventManager.fire(new ListInTransit());
			}

		});

		final JToggleButton buttonNotRegistered = new JToggleButton(
				messagesBundle.getString("buttonbar.button.notregistered"), IconUtil.getImageIcon(imagesBundle
						.getString("buttonbar.button.notregistered.icon")));
		buttonBar.add(buttonNotRegistered);
		group.add(buttonNotRegistered);
		buttonNotRegistered.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent event) {
				EventManager.fire(new ListUnregistered());
			}

		});

		final JToggleButton buttonRecent = new JToggleButton(messagesBundle.getString("buttonbar.button.recent"),
				IconUtil.getImageIcon(imagesBundle.getString("buttonbar.button.recent.icon")));
		buttonBar.add(buttonRecent);
		group.add(buttonRecent);
		buttonRecent.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent event) {
				EventManager.fire(new ListRecent());
			}

		});

		final JToggleButton buttonImportant = new JToggleButton(messagesBundle.getString("buttonbar.button.important"),
				IconUtil.getImageIcon(imagesBundle.getString("buttonbar.button.important.icon")));
		buttonBar.add(buttonImportant);
		group.add(buttonImportant);
		buttonImportant.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent event) {
				EventManager.fire(new ListImportants());
			}

		});

		final JToggleButton buttonLate = new JToggleButton(messagesBundle.getString("buttonbar.button.late"),
				IconUtil.getImageIcon(imagesBundle.getString("buttonbar.button.late.icon")));
		buttonBar.add(buttonLate);
		group.add(buttonLate);
		buttonLate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent event) {
				EventManager.fire(new ListNotDelivered());
			}

		});
		return scrollPane;
	}

}
