package com.alienlabz.encomendaz.factory;

import java.util.ResourceBundle;

/**
 * Fábrica de objetos de ResourceBundle.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class ResourceBundleFactory {

	/**
	 * Obter o bundle que contém informações sobre as imagens do sistema.
	 * 
	 * @return ResourceBundle de imagens.
	 */
	public static ResourceBundle getImagesBundle() {
		return ResourceBundle.getBundle("images");
	}

	/**
	 * Obter o bundle que contém informações sobre as mensagens exibidas pelo sistema.
	 * 
	 * @return ResourceBundle de mensagens.
	 */
	public static ResourceBundle getMessagesBundle() {
		return ResourceBundle.getBundle("messages");
	}

	/**
	 * Obter o bundle que contém informações sobre o sistema.
	 * 
	 * @return ResourceBundle do sistema.
	 */
	public static ResourceBundle getSystemBundle() {
		return ResourceBundle.getBundle("system");
	}

}
