package com.alienlabz.encomendaz.factory;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javax.enterprise.util.AnnotationLiteral;

import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.JRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority;
import org.pushingpixels.flamingo.api.ribbon.RibbonTask;
import org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.events.ChangeDefaults;
import com.alienlabz.encomendaz.bus.events.ChangePrefs;
import com.alienlabz.encomendaz.bus.events.ShowAbout;
import com.alienlabz.encomendaz.bus.events.ShowBackupWindow;
import com.alienlabz.encomendaz.bus.events.ShowBatchWindow;
import com.alienlabz.encomendaz.bus.events.ShowExport;
import com.alienlabz.encomendaz.bus.events.ShowPriceWindow;
import com.alienlabz.encomendaz.bus.events.ShowReportsWindow;
import com.alienlabz.encomendaz.bus.events.ShowSearchForm;
import com.alienlabz.encomendaz.bus.qualifiers.ListingRequired;
import com.alienlabz.encomendaz.bus.qualifiers.NewRequired;
import com.alienlabz.encomendaz.configuration.InternalConfiguration;
import com.alienlabz.encomendaz.domain.Person;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.util.IconUtil;

/**
 * Classe responsável apenas em montar o Ribbon da aplicação.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class RibbonFactory {
	private JRibbon ribbon;
	private ResourceBundle messagesBundle;
	private ResourceBundle imagesBundle;
	private Dimension defaultDimension = null;

	public JRibbon build() {
		messagesBundle = ResourceBundleFactory.getMessagesBundle();
		imagesBundle = ResourceBundleFactory.getImagesBundle();
		initializeRibbon();
		return ribbon;
	}

	private void initializeMainTask() {
		final RibbonTask task = new RibbonTask(messagesBundle.getString("ribbon.task.main"),
				initializeMainTaskBandTrackings(), initializeMainTaskBandPeople(), initializeMainTaskBandAbout());
		ribbon.addTask(task);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private AbstractRibbonBand<?> initializeMainTaskBandAbout() {
		final JRibbonBand band = new JRibbonBand(messagesBundle.getString("ribbon.task.band.about"), null);
		band.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(band.getControlPanel())));

		//		final JCommandButton buttonDocs = new JCommandButton(messagesBundle.getString("ribbon.task.band.button.docs"),
		//				IconUtil.getResizableIcon(imagesBundle.getString("ribbon.task.band.button.docs.icon"),
		//						InternalConfiguration.getDefaultRibbonIconDimension()));
		//		buttonDocs.setPreferredSize(defaultDimension);
		//		band.addCommandButton(buttonDocs, RibbonElementPriority.TOP);

		final JCommandButton buttonAbout = new JCommandButton(
				messagesBundle.getString("ribbon.task.band.button.about"), IconUtil.getResizableIcon(
						imagesBundle.getString("ribbon.task.band.button.about.icon"),
						InternalConfiguration.getDefaultRibbonIconDimension()));
		buttonAbout.setPreferredSize(defaultDimension);
		buttonAbout.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				EventManager.fire(new ShowAbout());
			}

		});
		band.addCommandButton(buttonAbout, RibbonElementPriority.TOP);

		return band;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private JRibbonBand initializeMainTaskBandPeople() {
		final JRibbonBand bandPeople = new JRibbonBand(messagesBundle.getString("ribbon.task.band.person"), null);
		final JCommandButton buttonPeople = new JCommandButton(
				messagesBundle.getString("ribbon.task.band.button.persons"), IconUtil.getResizableIcon(
						imagesBundle.getString("ribbon.task.band.button.persons.icon"),
						InternalConfiguration.getDefaultRibbonIconDimension()));
		bandPeople.addCommandButton(buttonPeople, RibbonElementPriority.TOP);
		buttonPeople.setPreferredSize(defaultDimension);
		buttonPeople.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				EventManager.fire(new Person(), new AnnotationLiteral<ListingRequired>() {
				});
			}

		});

		final JCommandButton buttonDefault = new JCommandButton(
				messagesBundle.getString("ribbon.task.band.button.default"), IconUtil.getResizableIcon(
						imagesBundle.getString("ribbon.task.band.button.default.icon"),
						InternalConfiguration.getDefaultRibbonIconDimension()));
		buttonDefault.setPreferredSize(defaultDimension);
		bandPeople.addCommandButton(buttonDefault, RibbonElementPriority.TOP);
		buttonDefault.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				EventManager.fire(new ChangeDefaults());
			}

		});

		bandPeople.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandPeople
				.getControlPanel())));
		return bandPeople;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private JRibbonBand initializeMainTaskBandTrackings() {
		final JRibbonBand bandTrackings = new JRibbonBand(messagesBundle.getString("ribbon.task.band.main"), null);

		bandTrackings.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandTrackings
				.getControlPanel())));

		// Botão para exibir formulário de cadastro completo de postagem.
		final JCommandButton buttonNew = new JCommandButton(messagesBundle.getString("ribbon.task.band.button.new"),
				IconUtil.getResizableIcon(imagesBundle.getString("ribbon.task.band.button.new.icon"),
						InternalConfiguration.getDefaultRibbonIconDimension()));
		buttonNew.setPreferredSize(defaultDimension);
		buttonNew.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				EventManager.fire(new Tracking(), new AnnotationLiteral<NewRequired>() {
				});
			}
		});
		bandTrackings.addCommandButton(buttonNew, RibbonElementPriority.TOP);

		// Botão para exibir formulário de cadastro de postagens por lote.
		final JCommandButton buttonBatch = new JCommandButton(
				messagesBundle.getString("ribbon.task.band.button.batch"), IconUtil.getResizableIcon(
						imagesBundle.getString("ribbon.task.band.button.batch.icon"),
						InternalConfiguration.getDefaultRibbonIconDimension()));
		buttonBatch.setPreferredSize(defaultDimension);
		bandTrackings.addCommandButton(buttonBatch, RibbonElementPriority.TOP);
		buttonBatch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				EventManager.fire(new ShowBatchWindow());
			}
		});

		final JCommandButton buttonSearch = new JCommandButton(
				messagesBundle.getString("ribbon.task.band.button.search"), IconUtil.getResizableIcon(
						imagesBundle.getString("ribbon.task.band.button.search.icon"),
						InternalConfiguration.getDefaultRibbonIconDimension()));
		buttonSearch.setPreferredSize(defaultDimension);
		bandTrackings.addCommandButton(buttonSearch, RibbonElementPriority.TOP);
		buttonSearch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				EventManager.fire(new ShowSearchForm());
			}
		});

		final JCommandButton buttonPrint = new JCommandButton(
				messagesBundle.getString("ribbon.task.band.button.print"), IconUtil.getResizableIcon(
						imagesBundle.getString("ribbon.task.band.button.print.icon"),
						InternalConfiguration.getDefaultRibbonIconDimension()));
		buttonPrint.setPreferredSize(defaultDimension);
		buttonPrint.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				EventManager.fire(new ShowReportsWindow());
			}

		});

		bandTrackings.addCommandButton(buttonPrint, RibbonElementPriority.TOP);

		final JCommandButton buttonCalculator = new JCommandButton(
				messagesBundle.getString("ribbon.task.band.button.calculator"), IconUtil.getResizableIcon(
						imagesBundle.getString("ribbon.task.band.button.calculator.icon"),
						InternalConfiguration.getDefaultRibbonIconDimension()));
		buttonCalculator.setPreferredSize(defaultDimension);
		bandTrackings.addCommandButton(buttonCalculator, RibbonElementPriority.TOP);
		buttonCalculator.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				EventManager.fire(new ShowPriceWindow());
			}
		});

		final JCommandButton buttonBackup = new JCommandButton(
				messagesBundle.getString("ribbon.task.band.button.backup"), IconUtil.getResizableIcon(
						imagesBundle.getString("ribbon.task.band.button.backup.icon"),
						InternalConfiguration.getDefaultRibbonIconDimension()));
		bandTrackings.addCommandButton(buttonBackup, RibbonElementPriority.TOP);
		buttonBackup.setPreferredSize(defaultDimension);
		buttonBackup.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EventManager.fire(new ShowBackupWindow());
			}

		});
		return bandTrackings;
	}

	private void initializeRibbon() {
		ribbon = new JRibbon();
		initializeMainTask();
		initializeToolsTask();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initializeToolsTask() {

		// Setup.
		final JRibbonBand bandSetup = new JRibbonBand(messagesBundle.getString("ribbon.task.band.tools.setup"), null);
		bandSetup
				.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandSetup.getControlPanel())));

		final JCommandButton buttonGeneralPrefs = new JCommandButton(
				messagesBundle.getString("ribbon.task.band.button.general"), IconUtil.getResizableIcon(
						imagesBundle.getString("ribbon.task.band.button.general.icon"),
						InternalConfiguration.getDefaultRibbonIconDimension()));
		bandSetup.addCommandButton(buttonGeneralPrefs, RibbonElementPriority.TOP);
		buttonGeneralPrefs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				EventManager.fire(new ChangePrefs());
			}
		});

		// Export.
		final JRibbonBand bandExport = new JRibbonBand(messagesBundle.getString("ribbon.task.band.tools.export"), null);
		bandExport.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandExport
				.getControlPanel())));

		final JCommandButton buttonExport = new JCommandButton(
				messagesBundle.getString("ribbon.task.band.tools.button.export"), IconUtil.getResizableIcon(
						imagesBundle.getString("ribbon.task.band.tools.button.export.icon"),
						InternalConfiguration.getDefaultRibbonIconDimension()));

		bandExport.addCommandButton(buttonExport, RibbonElementPriority.TOP);
		buttonExport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				EventManager.fire(new ShowExport());
			}
		});

		final RibbonTask task = new RibbonTask(messagesBundle.getString("ribbon.task.tools"), bandSetup, bandExport);
		ribbon.addTask(task);
	}
}
